CREATE VIEW view_login AS
SELECT A.*, B.institute_name
FROM tbl_login A
LEFT JOIN tbl_institute B ON A.institute_id = B.institute_id

//CREATE VIEW view_inmate AS
//SELECT A.*, B.institute_name, TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age 
//FROM tbl_inmate A
//LEFT JOIN tbl_institute B ON A.institute = B.institute_id

CREATE VIEW view_inmate AS
SELECT A.*, B.institute_name, TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age, C.institute_name AS transfered_institute_name
FROM tbl_inmate A
LEFT JOIN tbl_institute B ON A.institute = B.institute_id
LEFT JOIN tbl_institute C ON A.transfered_institute = C.institute_id

CREATE VIEW view_inmate_offence AS
SELECT A.*, B.offence
FROM tbl_inmate_offence A
JOIN tbl_offence B ON A.offence_id = B.offence_id

CREATE VIEW view_inmate_court AS
SELECT A.*, B.court_name
FROM tbl_inmate_court A
JOIN tbl_court B ON A.court_id = B.court_id

CREATE VIEW view_release AS
SELECT A.*, B.release_type
FROM tbl_release A
JOIN tbl_release_type B ON A.release_type_id = B.release_type_id

CREATE VIEW view_removed_inmate AS 
SELECT A.*, B.release_id, B.release_type_id, B.delete_date, B.return_date, B.remark, B.release_type
FROM view_inmate A
JOIN view_release B ON A.inmate_id = B.inmate_id

CREATE VIEW view_count_institute AS 
SELECT institute, institute_name, COUNT(institute_name) AS amount 
FROM view_inmate 
WHERE exchange = 0 AND active_state = 1 
GROUP BY institute_name

CREATE VIEW view_age_range AS 
SELECT inmate_id,dup_inmate_number,inmate_name,gender,inmate_category,prison_frequency,literacy,appeal,special,detention_order,religion,institute,exchange,institute_name,birthday,age,
    (CASE
        WHEN age BETWEEN 16 and 21 THEN '16 - 22'
        WHEN age BETWEEN 22 and 29 THEN '22 - 30'
        WHEN age BETWEEN 30 and 39 THEN '30 - 40'
        WHEN age BETWEEN 40 and 49 THEN '40 - 50'
        WHEN age BETWEEN 50 and 59 THEN '50 - 60'
        WHEN age BETWEEN 60 and 69 THEN '60 - 70'
        WHEN age >= 70 THEN '70+'
    END) AS 'range'
    FROM view_inmate WHERE active_state = 1 AND birthday <> ''
	

SELECT `range`, COUNT(*) AS amount FROM `view_age_range` WHERE `range` <> '' GROUP BY `range` ORDER BY `range` ASC 


CREATE VIEW view_inmate_report AS
SELECT A.*, GROUP_CONCAT(DISTINCT B.court_name) as court_name, GROUP_CONCAT(DISTINCT C.offence) as offence, 
(CASE
        WHEN age BETWEEN 16 and 21 THEN '16 - 22'
        WHEN age BETWEEN 22 and 29 THEN '22 - 30'
        WHEN age BETWEEN 30 and 39 THEN '30 - 40'
        WHEN age BETWEEN 40 and 49 THEN '40 - 50'
        WHEN age BETWEEN 50 and 59 THEN '50 - 60'
        WHEN age BETWEEN 60 and 69 THEN '60 - 70'
        WHEN age >= 70 THEN '70+'
    END) AS 'age_range'
FROM view_inmate A
LEFT JOIN view_inmate_court B ON A.inmate_id = B.inmate_id
LEFT JOIN view_inmate_offence C ON A.inmate_id = C.inmate_id
GROUP BY inmate_id;
