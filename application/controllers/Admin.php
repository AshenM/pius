<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {

    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('date');
		$this->load->library('form_validation');
		$this->load->model("gen_model");
    date_default_timezone_set('Asia/Colombo');
		//destroy already created session data
    if ($this->session->has_userdata('loggedin_user') == false) {
			redirect(base_url());
    }
  }

	//view active and current inmates
	public function dashboard(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }

		$table1='view_inmate';
		$table2='view_count_institute';
		$select1='institute_name';
		$select2='*';
		$where1=[
			'exchange'=>'0',
			'active_state'=>'1'
		];
		$where2=[
			'exchange'=>'1',
			'active_state'=>'1'
		];
		$result=$this->gen_model->get_data($select1,$table1,$where1);
		$data['total_inmates']=$result->num_rows();
		$result1=$this->gen_model->get_data($select1,$table1,$where2);
		$data['total_intransit']=$result1->num_rows();

		$result2=$this->gen_model->get_data($select2,$table2);
		$data['total_institutes']=$result2->result();

		//get chart data

		//for Inmates by Gender
		$query1="SELECT gender, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `gender` ";
		$result3=$this->gen_model->gen_query($query1);
		$data['gender_chart']=$result3->result();
		//for Classification of Prisoners
		$query2="SELECT `inmate_category`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1  GROUP BY `inmate_category` ";
		$result4=$this->gen_model->gen_query($query2);
		$data['inmatecat_chart']=$result4->result();
		//for Frequency of incarceration
		$query3="SELECT `prison_frequency`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `prison_frequency` ";
		$result5=$this->gen_model->gen_query($query3);
		$data['prison_frequency_chart']=$result5->result();
		//for Appeal and Non-appeal Inmates
		$query4="SELECT `appeal`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `appeal` ORDER BY `appeal` ASC";
		$result6=$this->gen_model->gen_query($query4);
		$data['appeal_chart']=$result6->result();
		//for Special and Normal Inmates
		$query5="SELECT `special`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `special` ORDER BY `special` ASC ";
		$result7=$this->gen_model->gen_query($query5);
		$data['special_chart']=$result7->result();
		//for Literacy of Inmates
		$query5="SELECT `literacy`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `literacy` ORDER BY `literacy` ASC ";
		$result7=$this->gen_model->gen_query($query5);
		$data['literacy_chart']=$result7->result();
		//for Restrained on detention orders
		$query6="SELECT `detention_order`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `detention_order` ORDER BY `detention_order` ASC ";
		$result8=$this->gen_model->gen_query($query6);
		$data['detention_chart']=$result8->result();
		//for Inmates by Nationality
		$query7="SELECT `country`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `country` ORDER BY `country` DESC ";
		$result9=$this->gen_model->gen_query($query7);
		$data['country_chart']=$result9->result();
		//for Inmates by Nation
		$query8="SELECT `nationality`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `nationality` ORDER BY `nationality` DESC ";
		$result10=$this->gen_model->gen_query($query8);
		$data['nation_chart']=$result10->result();
		//for Inmates by Religion
		$query9="SELECT `religion`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 GROUP BY `religion` ";
		$result11=$this->gen_model->gen_query($query9);
		$data['religion_chart']=$result11->result();
		//for Inmates by Age
		$query9="SELECT `range`, COUNT(*) AS amount FROM `view_age_range` WHERE `range` <> '' GROUP BY `range` ORDER BY `range` ASC";
		$result11=$this->gen_model->gen_query($query9);
		$data['age_chart']=$result11->result();

		//var_dump($data1);die;
		$this->load->view('admin/dashboard',$data);
	}

	//view active Inmates
	public function active_inmates(){
			//data to load on select options
		$table1="tbl_institute";//table name to retrieve institute data
		$table2='tbl_offence';
		$table3='tbl_court';
		$table4='tbl_release_type';
		$select='*';
		//load gearic modal to get data
		$result1=$this->gen_model->get_data($select,$table1);
		$data['institutes']=$result1->result();
		$result2=$this->gen_model->get_data($select,$table2);
		$data['offences']=$result2->result();
		$result3=$this->gen_model->get_data($select,$table3);
		$data['courts']=$result3->result();
		$result4=$this->gen_model->get_data($select,$table4);
		$data['release']=$result4->result();

		//data to load on the table
		$table3="view_inmate";
    $select2= 'inmate_id,dup_inmate_number,inmate_name,institute_name,prison_date,inmate_category';
		$where=[
			//'institute'=> $ses_institute_id,
			'enter_by'=> $ses_login_id,
			'active_state'=> '1',
			'exchange' => '0'
		];
    $result3 = $this->gen_model->get_data($select2,$table3,$where);
    $data['inmates']=$result3->result();
		//var_dump($data);die;
		$this->load->view('data_entry/view_inmates',$data);
	}

	//view active and current inmates
	public function single_institute($param=''){
		$id=$param;
		if($id == ''){
			redirect(base_url(). "admin/dashboard");
		}else{
			$today=date('Y-m-d');
			$table1='view_inmate';
			$table2='view_count_institute';
			$table3='view_removed_inmate';
			$table4='tbl_institute';
			$select1='institute_name';
			$select2='*';
			$select3='transfered_institute_name';
			$where1=[
				'institute'=>$id,
				'exchange'=>'0',
				'active_state'=>'1'
			];
			$where2=[
				'institute'=>$id,
				'exchange'=>'1',
				'active_state'=>'1'
			];
			$where3=[
				'transfered_institute'=>$id,
				'exchange_date'=>$today,
				'active_state'=>'1'
			];
			$where4=[
				'institute'=>$id,
				'exchange_date'=>$today,
				'active_state'=>'1'
			];
			$where5=[
				'institute'=>$id,
				'release_type_id !=' => '7',
				'delete_date'=>$today,
				'active_state'=>'0'
			];
			$where6=[
				'institute'=>$id,
				'release_type_id =' => '7',
				'delete_date'=>$today,
				'active_state'=>'0'
			];
			$where7=[
				'institute_id'=>$id
			];

			//total inmates
			$result=$this->gen_model->get_data($select1,$table1,$where1);
			$data['total_inmates']=$result->num_rows();
			//intransit inmates
			$result1=$this->gen_model->get_data($select3,$table1,$where2);
			$data['total_intransit']=$result1->num_rows();

			//for transfered inmates
			$result13=$this->gen_model->get_data($select1,$table1,$where3);
			$data['transfered_inmates']=$result13->num_rows();
			//for recieved inmates
			$result14=$this->gen_model->get_data($select1,$table3,$where4);
			$data['recieved_inmates']=$result14->num_rows();

			//for released inmates
			$result15=$this->gen_model->get_data($select1,$table3,$where5);
			$data['released_inmates']=$result15->num_rows();
			$result16=$this->gen_model->get_data($select1,$table3,$where6);
			$data['dead_inmates']=$result16->num_rows();

			//get institute_name
			$result17=$this->gen_model->get_data($select1,$table4,$where7);
			foreach($result17->result() as $row){
				$data['institute'] =$row->institute_name;
			}
			//var_dump($data);die;
			//get chart data

			//for Inmates by Gender
			$query1="SELECT gender, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `gender` ";
			//var_dump($query1);die;
			$result3=$this->gen_model->gen_query($query1);
			$data['gender_chart']=$result3->result();
			//for Classification of Prisoners
			$query2="SELECT `inmate_category`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `inmate_category` ";
			$result4=$this->gen_model->gen_query($query2);
			$data['inmatecat_chart']=$result4->result();
			//for Frequency of incarceration
			$query3="SELECT `prison_frequency`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `prison_frequency` ";
			$result5=$this->gen_model->gen_query($query3);
			$data['prison_frequency_chart']=$result5->result();
			//for Appeal and Non-appeal Inmates
			$query4="SELECT `appeal`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `appeal` ORDER BY `appeal` ASC";
			$result6=$this->gen_model->gen_query($query4);
			$data['appeal_chart']=$result6->result();
			//for Special and Normal Inmates
			$query5="SELECT `special`, COUNT(*) AS amount FROM `view_inmate` WHERE  `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `special` ORDER BY `special` ASC ";
			$result7=$this->gen_model->gen_query($query5);
			$data['special_chart']=$result7->result();
			//for Literacy of Inmates
			$query5="SELECT `literacy`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `literacy` ORDER BY `literacy` ASC ";
			$result7=$this->gen_model->gen_query($query5);
			$data['literacy_chart']=$result7->result();
			//for Restrained on detention orders
			$query6="SELECT `detention_order`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `detention_order` ORDER BY `detention_order` ASC ";
			$result8=$this->gen_model->gen_query($query6);
			$data['detention_chart']=$result8->result();
			//for Inmates by Nationality
			$query7="SELECT `country`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `country` ORDER BY `country` DESC ";
			$result9=$this->gen_model->gen_query($query7);
			$data['country_chart']=$result9->result();
			//for Inmates by Nation
			$query8="SELECT `nationality`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `nationality` ORDER BY `nationality` DESC ";
			$result10=$this->gen_model->gen_query($query8);
			$data['nation_chart']=$result10->result();
			//for Inmates by Religion
			$query9="SELECT `religion`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$id." GROUP BY `religion` ";
			$result11=$this->gen_model->gen_query($query9);
			$data['religion_chart']=$result11->result();
			//for Inmates by Age
			$query10="SELECT `range`, COUNT(*) AS amount FROM `view_age_range` WHERE `range` <> '' AND `exchange`=0 AND `institute`=".$id." GROUP BY `range` ORDER BY `range` ASC";
			$result12=$this->gen_model->gen_query($query10);
			$data['age_chart']=$result12->result();
			//var_dump($data);die;
			$this->load->view('admin/single_institute',$data);
		}

	}

	//view progress Summary
	public function progress_summary(){
		$today=date('Y-m-d');

		//today enter amount
		$query1="SELECT `institute_name`, COUNT(*) AS amount FROM `view_inmate` WHERE `enter_date`=".$today." AND `active_state`=1 GROUP BY `institute_name` ";
		$result1=$this->gen_model->gen_query($query1);
		$data['today_enter']=$result1->result();

		//today recieved amount
		$query2="SELECT `institute_name`, COUNT(*) AS amount FROM `view_inmate` WHERE `exchange_date`=".$today." AND `active_state`=1 GROUP BY `institute_name` ";
		$result2=$this->gen_model->gen_query($query2);
		$data['today_recieved']=$result2->result();

		//today transfered amount
		$query3="SELECT `transfered_institute_name`, COUNT(*) AS amount FROM `view_inmate` WHERE `exchange_date`=".$today." AND `active_state`=1 GROUP BY `transfered_institute_name` ";
		$result3=$this->gen_model->gen_query($query3);
		$data['today_transfered']=$result3->result();

		//today death amount
		$query4="SELECT `institute_name`, COUNT(*) AS amount FROM `view_removed_inmate` WHERE `release_type_id`=7 AND `delete_date`=".$today." GROUP BY `institute_name` ";
		$result4=$this->gen_model->gen_query($query4);
		$data['today_death']=$result4->result();

		//today release amount
		$query5="SELECT `institute_name`, COUNT(*) AS amount FROM `view_removed_inmate` WHERE `release_type_id`!=7 AND `delete_date`=".$today." GROUP BY `institute_name` ";
		$result5=$this->gen_model->gen_query($query5);
		$data['today_release']=$result5->result();

		//today intransit amount
		$query6="SELECT `institute_name`, COUNT(*) AS amount FROM `view_inmate` WHERE `exchange`=1 AND `active_state`=1 GROUP BY `institute_name` ";
		$result6=$this->gen_model->gen_query($query6);
		$data['intransit']=$result6->result();

		//var_dump($data);die;
		$this->load->view('admin/progress_summary',$data);
	}

}
