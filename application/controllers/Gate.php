<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gate extends CI_Controller {

	public function __construct() {

    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('date');
		$this->load->library('form_validation');
		$this->load->model("gen_model");
    date_default_timezone_set('Asia/Colombo');
		//destroy already created session data
    if ($this->session->has_userdata('loggedin_user') == false) {
			redirect(base_url());
    }
  }

	//gate dashboard
	public function dashboard(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }else{
			redirect(base_url() . "login"); // no session
		}
		$this->load->view('gate/gate_dashboard');

	}

	//search inmates
	public function search_inmates(){
		$table="view_inmate";
		$search_option= $_POST['search_option'];
		$search_data= $_POST['search_data'];
		//$search_option= 'dup_inmate_number';
		//$search_data= 'z';
		$where=[
			$search_option =>$search_data
		];
		$select= 'dup_inmate_number,inmate_name,institute_name,prison_date,active_state';
		$query="SELECT ".$select." FROM ".$table." WHERE  ".$search_option." LIKE '%".$search_data."%' AND `active_state`=1";
		$result = $this->gen_model->gen_query($query);
		//var_dump($query);
		$table = '';
		$table .= '
			<table id="tbl_inmates" class="table table-bordered table-striped table-responsive">
                    <thead>
                      <tr>
                        <th>සිර අංකය</th>
                        <th>නම</th>
                        <th>බන්ධනාගාර ගත වූ දිනය</th>
                        <th>දැනට සිටින බන්ධනාගාර ආයතනය</th>
                      </tr>
                    </thead>
                    <tbody>                    
		';

		if(!empty($result)){
			foreach ($result->result() as $row) {
			$table .= '
				<tr>
					<td>'.$row->dup_inmate_number.'</td>
					<td>'.$row->inmate_name.'</td>
					<td>'.$row->prison_date.'</td>
					<td>'.$row->institute_name.'</td>
				</tr>';
			}
			$table .= '
				</tbody>
                  </table>
			';
			//var_dump($table);
			echo json_encode($table);
		}else{
			echo json_encode('false');
		}
	}

}
