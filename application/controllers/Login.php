<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('date');
		$this->load->library('form_validation');
    date_default_timezone_set('Asia/Colombo');
  }

	//load login page
	public function index()	{
		//destroy already created session data
    if ($this->session->has_userdata('loggedin_user') == TRUE) {
        $this->session->unset_userdata("loggedin_user");
        $this->session->sess_destroy("loggedin_user");
    }
		$this->load->view('login_page');
	}

	//verifying login attempt
	public function check_login()	{
		//destroy already created session data
    if ($this->session->has_userdata('loggedin_user') == TRUE) {
        $this->session->unset_userdata("loggedin_user");
        $this->session->sess_destroy("loggedin_user");
    }
		$username=strtolower($this->input->post("username"));//Converterting to lowercase
		$password=sha1($this->input->post("password"));
		$login_data=array(
      "username"=>$username,
      "password"=>$password
    );
    $table='view_login'; // table name
    //checking for availability of username and password
    $this->load->model("gen_model");
    $key_result=$this->gen_model->key_search($login_data,$table);
    //if credentials exists check whether the user is active or not
		if($key_result->num_rows() == 1){
			//getting the employee id of the login user to check the user activeness
			foreach($key_result->result() as $row){
				$login_state = $row->login_state;
			}

			if($login_state != '1'){
				$this->session->set_flashdata('error', 'සමාවන්න! ඔබට ඇතුල්වීමට අවසර නැත.');
				redirect(base_url() . "login"); // unsuccess
			}else{
				//create session
				foreach($key_result->result() as $row){
					$loggedin_user=array(
						"ses_login_id" => $row->login_id,
						"ses_user" => $row->user,
						"ses_username" => $row->username,
						"ses_institute_id" => $row->institute_id,
						"ses_institute_name" => $row->institute_name,
						"ses_user_type" => $row->user_type
					);
				}
				//var_dump($loggedin_user);die;
				$this->session->set_userdata('loggedin_user', $loggedin_user);
				//var_dump($loggedin_user);die;
				$this->check_user_type();
			}
		}else{
			$this->session->set_flashdata('error', 'ඔබගේ පරිශීලක නාමය හෝ මුර පදය වැරදියි !');
			redirect(base_url()); //error
		}

	}

	public function check_user_type(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
			$session_data=$this->session->userdata('loggedin_user');
			$ses_user_type=$session_data['ses_user_type'];
			//var_dump($ses_user_type);die;

			if($ses_user_type == "Admin"){ // for applicant
				redirect(base_url(). "admin/dashboard");
			}else if($ses_user_type == "Data Entry"){
				redirect(base_url(). "inmates/dashboard");
			}else if($ses_user_type == "Gate"){
				redirect(base_url(). "gate/dashboard");
			}
    }else{
			$this->session->set_flashdata('error', 'පද්ධතියට ප්‍රවේශ වීමට කරුණාකර පූරනය වන්න.');
			redirect(base_url()); //error
		}
	}

}
