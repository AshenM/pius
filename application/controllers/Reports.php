<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() {

    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('date');
		$this->load->library('form_validation');
		$this->load->model("gen_model");
    date_default_timezone_set('Asia/Colombo');
		//destroy already created session data
    if ($this->session->has_userdata('loggedin_user') == false) {
			redirect(base_url());
    }
  }

	//view active and current inmates
	public function tabular_report(){

		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_institute_id=$session_data['ses_institute_id'];
			$ses_user_type=$session_data['ses_user_type'];
	  }

		$table="view_inmate_report ";//table name to retrieve institute data
		$select='inmate_id,dup_inmate_number,inmate_name,gender,prison_date,release_date,release_date_rem,prisoned_age,no_of_cases,
			inmate_category,prison_frequency,literacy,appeal,special,detention_order,country,nationality,religion,birthday,institute_name,age,court_name,offence,age_range'; //data to load on select options
		$where=[
			'institute'=>$ses_institute_id,
			'exchange'=>'0',
			'active_state'=> '1'
		];
		//if the user is admin. get all the data
		if($ses_user_type =='Admin'){
			$where=[
				'active_state'=> '1'
			];
		}

		//load gearic modal to get data
		$result=$this->gen_model->get_data($select,$table,$where);
		$data['inmates']=$result->result();

		//var_dump($data);die;
		$this->load->view('reports/tabular_report',$data);
	}

	//view active and current inmates
	public function transfer_report(){

		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_institute_id=$session_data['ses_institute_id'];
			$ses_user_type=$session_data['ses_user_type'];
	  }

		$table1="tbl_institute";//table name to retrieve institute data
		$select1='institute_id,institute_name'; //data to load on select options
		$where1=[
			'active_state'=> '1'
		];

		//load gearic modal to get data
		$result=$this->gen_model->get_data($select1,$table1,$where1);
		$data['institutes']=$result->result();
		$data['transfers']=null;
		$data['feedback_data']=null;
		$this->form_validation->set_rules('from_institute', 'පෙර සිටි ආයතනය', 'trim|required');
		$this->form_validation->set_rules('to_institute', 'මාරු වූ ආයතනය', 'trim|required');
		if($this->form_validation->run()== true){
			$table2="view_inmate_report";
			$select2='inmate_id,dup_inmate_number,inmate_name,gender,prison_date,release_date,release_date_rem,prisoned_age,no_of_cases,exchange_date,
				inmate_category,prison_frequency,literacy,appeal,special,detention_order,country,nationality,religion,birthday,institute_name,age,court_name,offence,age_range'; //data to load on select options
			$where2=[
				"transfered_institute"=>$this->input->post("from_institute"),
				"institute"=>$this->input->post("to_institute"),
				"active_state"=> '1'
			];
			$this->form_validation->set_rules('from_date', 'සිට (දිනය)', 'trim|required');
			$this->form_validation->set_rules('to_date', 'තෙක් (දිනය)', 'trim|required');
			$data['feedback_data']=[
				"from_institute"=>$this->input->post("from_institute"),
				"to_institute"=>$this->input->post("to_institute"),
				"from_date"=>null,
				"to_date"=>null,
			];
			if($this->form_validation->run()== true){
				$where2=[
					"transfered_institute"=>$this->input->post("from_institute"),
					"institute"=>$this->input->post("to_institute"),
					"exchange_date >="=>$this->input->post("from_date"),
					"exchange_date <="=>$this->input->post("to_date"),
					"active_state"=> '1'
				];
				$data['feedback_data']=[
					"from_institute"=>$this->input->post("from_institute"),
					"to_institute"=>$this->input->post("to_institute"),
					"from_date"=>$this->input->post("from_date"),
					"to_date"=>$this->input->post("to_date"),
				];
			}

			$result=$this->gen_model->get_data($select2,$table2,$where2);
			$data['transfers']=$result->result();
		}

		//var_dump($data);die;
		$this->load->view('reports/transfer_report',$data);
	}



}
