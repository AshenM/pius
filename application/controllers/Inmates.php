<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inmates extends CI_Controller {

	public function __construct() {

    parent::__construct();
    $this->load->library('session');
    $this->load->helper('url');
    $this->load->helper('date');
		$this->load->library('form_validation');
		$this->load->model("gen_model");
    date_default_timezone_set('Asia/Colombo');
		//destroy already created session data
    if ($this->session->has_userdata('loggedin_user') == false) {
			redirect(base_url());
    }
  }

	//data entry dashboard
	public function dashboard(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }

		$today=date('Y-m-d');
		$table1='view_inmate';
		$table2='view_count_institute';
		$table3='view_removed_inmate';
		$select1='institute_name';
		$select2='*';
		$select3='transfered_institute_name';
		$where1=[
			'institute'=>$ses_institute_id,
			'exchange'=>'0',
			'active_state'=>'1'
		];
		$where2=[
			'institute'=>$ses_institute_id,
			'exchange'=>'1',
			'active_state'=>'1'
		];
		$where3=[
			'transfered_institute'=>$ses_institute_id,
			'exchange_date'=>$today,
			'active_state'=>'1'
		];
		$where4=[
			'institute'=>$ses_institute_id,
			'exchange_date'=>$today,
			'active_state'=>'1'
		];
		$where5=[
			'institute'=>$ses_institute_id,
			'release_type_id !=' => '7',
			'delete_date'=>$today,
			'active_state'=>'0'
		];
		$where6=[
			'institute'=>$ses_institute_id,
			'release_type_id =' => '7',
			'delete_date'=>$today,
			'active_state'=>'0'
		];

		//total inmates
		$result=$this->gen_model->get_data($select1,$table1,$where1);
		$data['total_inmates']=$result->num_rows();
		//intransit inmates
		$result1=$this->gen_model->get_data($select3,$table1,$where2);
		$data['total_intransit']=$result1->num_rows();

		//for transfered inmates
		$result13=$this->gen_model->get_data($select1,$table1,$where3);
		$data['transfered_inmates']=$result13->num_rows();
		//for recieved inmates
		$result14=$this->gen_model->get_data($select1,$table3,$where4);
		$data['recieved_inmates']=$result14->num_rows();

		//for released inmates
		$result15=$this->gen_model->get_data($select1,$table3,$where5);
		$data['released_inmates']=$result15->num_rows();
		$result16=$this->gen_model->get_data($select1,$table3,$where6);
		$data['dead_inmates']=$result16->num_rows();
		//for recieved inmates

		//get chart data

		//for Inmates by Gender
		$query1="SELECT gender, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `gender` ";
		//var_dump($query1);die;
		$result3=$this->gen_model->gen_query($query1);
		$data['gender_chart']=$result3->result();
		//for Classification of Prisoners
		$query2="SELECT `inmate_category`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `inmate_category` ";
		$result4=$this->gen_model->gen_query($query2);
		$data['inmatecat_chart']=$result4->result();
		//for Frequency of incarceration
		$query3="SELECT `prison_frequency`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `prison_frequency` ";
		$result5=$this->gen_model->gen_query($query3);
		$data['prison_frequency_chart']=$result5->result();
		//for Appeal and Non-appeal Inmates
		$query4="SELECT `appeal`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `appeal` ORDER BY `appeal` ASC";
		$result6=$this->gen_model->gen_query($query4);
		$data['appeal_chart']=$result6->result();
		//for Special and Normal Inmates
		$query5="SELECT `special`, COUNT(*) AS amount FROM `view_inmate` WHERE  `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `special` ORDER BY `special` ASC ";
		$result7=$this->gen_model->gen_query($query5);
		$data['special_chart']=$result7->result();
		//for Literacy of Inmates
		$query5="SELECT `literacy`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `literacy` ORDER BY `literacy` ASC ";
		$result7=$this->gen_model->gen_query($query5);
		$data['literacy_chart']=$result7->result();
		//for Restrained on detention orders
		$query6="SELECT `detention_order`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `detention_order` ORDER BY `detention_order` ASC ";
		$result8=$this->gen_model->gen_query($query6);
		$data['detention_chart']=$result8->result();
		//for Inmates by Nationality
		$query7="SELECT `country`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `country` ORDER BY `country` DESC ";
		$result9=$this->gen_model->gen_query($query7);
		$data['country_chart']=$result9->result();
		//for Inmates by Nation
		$query8="SELECT `nationality`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `nationality` ORDER BY `nationality` DESC ";
		$result10=$this->gen_model->gen_query($query8);
		$data['nation_chart']=$result10->result();
		//for Inmates by Religion
		$query9="SELECT `religion`, COUNT(*) AS amount FROM `view_inmate` WHERE `active_state`=1 AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `religion` ";
		$result11=$this->gen_model->gen_query($query9);
		$data['religion_chart']=$result11->result();
		//for Inmates by Age
		$query10="SELECT `range`, COUNT(*) AS amount FROM `view_age_range` WHERE `range` <> '' AND `exchange`=0 AND `institute`=".$ses_institute_id." GROUP BY `range` ORDER BY `range` ASC";
		$result12=$this->gen_model->gen_query($query10);
		$data['age_chart']=$result12->result();

		//var_dump($data);die;
		$this->load->view('data_entry/dashboard',$data);
	}

	//load data insert form
	public function add_inmates(){
		//data to load on select options
		$table1="tbl_institute";//table name to retrieve institute data
		$table2='tbl_offence';
		$table3='tbl_court';
		$select='*';
		//load gearic modal to get data
		$result1=$this->gen_model->get_data($select,$table1);
		$data['institutes']=$result1->result();
		$result2=$this->gen_model->get_data($select,$table2);
		$data['offences']=$result2->result();
		$result3=$this->gen_model->get_data($select,$table3);
		$data['courts']=$result3->result();
		//var_dump($data);die;
		$this->load->view('data_entry/insert_inmates_form',$data);
	}

	//insert inmate to db
	public function insert_inmates(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }
		//form validation
		$this->form_validation->set_rules('institute', 'බන්ධනාගාර ආයතනය', 'trim|required');
		$this->form_validation->set_rules('inmate_number', 'සිර අංකය', 'trim|required');
		$this->form_validation->set_rules('inmate_name', 'නම', 'trim|required');
		//$this->form_validation->set_rules('birthday', 'උපන් දිනය', 'trim|required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('gender', 'ස්ත්‍රී පුරුෂ භාවය', 'trim|required');
		$this->form_validation->set_rules('prison_date', 'බන්ධනාගාර ගත වූ දිනය', 'trim|required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('release_date', 'නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් රහිතව)', 'trim|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('release_date_rem', 'නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් සහිතව)', 'trim|min_length[10]|max_length[10]');
		//$this->form_validation->set_rules('court', 'උසාවිය', 'trim|required');
		$this->form_validation->set_rules('no_of_cases', 'විභාග වන නඩු සංඛ්‍යාව', 'trim|required');
		$this->form_validation->set_rules('inmate_cat', 'සිරකරුවන් වර්ගීකරණය', 'trim|required');
		$this->form_validation->set_rules('prison_frequency', 'සිර ගත වූ වාර ප්‍රමාණය', 'trim|required');
		//$this->form_validation->set_rules('offence', 'වරද', 'trim|required');
		$this->form_validation->set_rules('literacy', 'සාක්ෂරතා හැකියාව', 'trim|required');
		$this->form_validation->set_rules('appeal', 'ඇපිල් සිරකරුවෙක්ද?', 'trim|required');
		$this->form_validation->set_rules('special', 'විශේෂ සිරකරුවෙක්ද?', 'trim|required');
		$this->form_validation->set_rules('detention_order', 'රුඳවුම් නියෝග මත රඳවා ඇත්ද?', 'trim|required');
		$this->form_validation->set_rules('country', 'ජාතිකත්වය', 'trim|required');
		$this->form_validation->set_rules('nationality', 'ජාතිය', 'trim|required');
		$this->form_validation->set_rules('religion', 'ආගම', 'trim|required');

		if($this->form_validation->run()== false){//returns true if false
			$error= validation_errors();
			$this->session->set_flashdata('error', $error);
			redirect(base_url() . "inmates/add_inmates");
		}else{
			//table name to save
			$table="tbl_inmate";
			//data to save in db
			$today=date('Y-m-d');
			$inmate_number=strtoupper($this->input->post("inmate_number")).'-'.$ses_institute_id;
			//change the inmate number for suspects
			$suspect=$this->input->post("suspect");
			if($suspect == '1'){
				$inmate_number=$inmate_number.'/'.date('Y');
			}
			$birthday=$this->input->post("birthday");
			$prison_date=$this->input->post("prison_date");

			$from = new DateTime($birthday);
			$to   = new DateTime($prison_date);
			$prisoned_age= $from->diff($to)->y;
			if($birthday == ''){
				$prisoned_age='';
			}
			$data=[
				"enter_by "=>$ses_login_id,
				"enter_date "=>$today,
				"institute"=>$this->input->post("institute"),
				"suspect"=>$this->input->post("suspect"),
				"inmate_number"=>$inmate_number,
				"dup_inmate_number"=>strtoupper($this->input->post("inmate_number")),
				"inmate_name"=>$this->input->post('inmate_name'),
				"birthday "=>$birthday,
				"prisoned_age"=>$prisoned_age,
				"gender"=>$this->input->post("gender"),
				"prison_date"=>$prison_date,
				"release_date"=>$this->input->post("release_date"),
				"release_date_rem"=>$this->input->post("release_date_rem"),
				//"court_id"=>$this->input->post("court"),
				"no_of_cases"=>$this->input->post("no_of_cases"),
				"inmate_category"=>$this->input->post("inmate_cat"),
				"prison_frequency"=>$this->input->post("prison_frequency"),
				"literacy"=>$this->input->post("literacy"),
				"appeal"=>$this->input->post("appeal"),
				"special"=>$this->input->post("special"),
				"detention_order"=>$this->input->post("detention_order"),
				"country"=>$this->input->post("country"),
				"nationality"=>$this->input->post("nationality"),
				"religion"=>$this->input->post("religion"),
				"last_update_date"=>$today
			];
			//checkling for duplicate inmate_number
			$key=[
					"dup_inmate_number"=>strtoupper($this->input->post("inmate_number"))
			];
			$key_result=$this->gen_model->key_search($key,$table);
			//$inmate_no=$key_result->result();
			if($key_result->num_rows() == 0){
				//if there's no duplicate valu, then insert data
				$result=$this->gen_model->insert_data_return_id($data,$table);
				if ($result) {
					//inserting the offence and inmate id to a different table
					$table2='tbl_inmate_offence';
					$table3='tbl_inmate_court';
					$offences=$this->input->post("offence");
					foreach($offences as $row1){
						$data2[]=[
							'inmate_id'=>$result,
							'offence_id'=>$row1
						];
					}
					$courts=$this->input->post("court");
					foreach($courts as $row2){
						$data3[]=[
							'inmate_id'=>$result,
							'court_id'=>$row2
						];
					}
					//var_dump($data2);die;
					$result2 = $this->gen_model->insert_multi_data($data2,$table2);
					$result3 = $this->gen_model->insert_multi_data($data3,$table3);
					if($result2 && $result3){
						$this->session->set_flashdata('success', 'දත්ත ඇතුලත් කිරීම සාර්ථකයි !');
						redirect(base_url() . "inmates/add_inmates"); //success
					}

				} else {
					$this->session->set_flashdata('error', 'සමාවන්න! දත්ත ඇතුලත් කිරීම අසාර්ථකයි  !');
					redirect(base_url() . "inmates/add_inmates"); // unsuccess
				}
			}else{
				$this->session->set_flashdata('error', 'සමාවන්න! ඔබ ඇතුලත් කල සිර අංකය දැනටමත් පවතී.');
				redirect(base_url() . "inmates/add_inmates");
			}
		}
	}

	//view active and current inmates
	public function view_inmates(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }

		//data to load on select options
		$table1="tbl_institute";//table name to retrieve institute data
		$table2='tbl_offence';
		$table3='tbl_court';
		$table4='tbl_release_type';
		$select='*';
		$where2=[
			'active_state'=> '1'
		];
		//load gearic modal to get data
		$result1=$this->gen_model->get_data($select,$table1,$where2);
		$data['institutes']=$result1->result();
		$result2=$this->gen_model->get_data($select,$table2);
		$data['offences']=$result2->result();
		$result3=$this->gen_model->get_data($select,$table3);
		$data['courts']=$result3->result();
		$result4=$this->gen_model->get_data($select,$table4);
		$data['release']=$result4->result();

		//data to load on the table
		$table3="view_inmate";
    $select2= 'inmate_id,dup_inmate_number,inmate_name,institute_name,prison_date,inmate_category';
		$where=[
			//'institute'=> $ses_institute_id,
			'enter_by'=> $ses_login_id,
			'active_state'=> '1',
			'exchange' => '0'
		];
    $result3 = $this->gen_model->get_data($select2,$table3,$where);
    $data['inmates']=$result3->result();
		//var_dump($data);die;
		$this->load->view('data_entry/view_inmates',$data);
	}

	//view single inmate on a modal AJAX
  public function view_single_inmate(){
    $table="view_inmate";
		$table2="view_inmate_offence";
		$table3="view_inmate_court";
    $key_arr['inmate_id']= $_POST['view_key'];
		//$key_arr['inmate_id']= '1';
		//$key_arr['emp_id']= '1';
    $where=$key_arr;
    $select= '*';
    $result = $this->gen_model->get_data($select,$table,$where);
		//var_dump($result);die;
		if(!empty($result)){
      foreach ($result->result() as $row) {
        $output[1]=array(
					"inmate_id"=>$row->inmate_id,
					"inmate_number"=>$row->dup_inmate_number,
					"inmate_name"=>$row->inmate_name,
					"institute_id"=>$row->institute,
					"institute"=>$row->institute_name,
					"birthday"=>$row->birthday,
					"prisoned_age"=>$row->prisoned_age,
					"gender"=>$row->gender,
					"prison_date"=>$row->prison_date,
					"release_date_rem"=>$row->release_date_rem,
					"release_date"=>$row->release_date,
					"no_of_cases"=>$row->no_of_cases,
					"inmate_category"=>$row->inmate_category,
					"prison_frequency"=>$row->prison_frequency,
					"literacy"=>$row->literacy,
					"appeal"=>$row->appeal,
					"special"=>$row->special,
					"detention_order"=>$row->detention_order,
					"country"=>$row->country,
					"nationality"=>$row->nationality,
					"religion"=>$row->religion,
					"exchange_date"=>$row->exchange_date
        );
      }
			foreach ($result->result() as $row) {
					$inmate_id=$row->inmate_id;
      }
			$where2=[
				"inmate_id"=>$inmate_id
			];
	    $result2 = $this->gen_model->get_data($select,$table2,$where2);
			$output[2][]=[];
			foreach ($result2->result() as $row) {
					$output[2][] = [
						'offence'=>$row->offence
					];
					$output[3][] = [
						'offence_id'=>$row->offence_id
					];
					$output[4][] = [
						'io_id'=>$row->io_id
					];
      }
			//var_dump($result2->result());
			$result3 = $this->gen_model->get_data($select,$table3,$where2);
			foreach ($result3->result() as $row2) {
					$output[5][] = [
						'court'=>$row2->court_name
					];
					$output[6][] = [
						'court_id'=>$row2->court_id
					];
					$output[7][] = [
						'ic_id'=>$row2->ic_id
					];
      }
			//var_dump($output);
      echo json_encode($output);
    }else{
      echo json_encode("සමාවන්න! මෙම රැඳවියන්ගේ තොරතුරු නොමැත.");
    }
  }

	//change the details of inmates, changing the inmate number and the institute also possible
	public function update_inmates(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }

		//form validation
		$this->form_validation->set_rules('inmate_id', 'Inmate Id', 'trim|required');
		$this->form_validation->set_rules('inmate_number', 'සිර අංකය', 'trim|required');
		$this->form_validation->set_rules('inmate_name', 'නම', 'trim|required');
		$this->form_validation->set_rules('birthday', 'උපන් දිනය', 'trim|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('gender', 'ස්ත්‍රී පුරුෂ භාවය', 'trim|required');
		$this->form_validation->set_rules('prison_date', 'බන්ධනාගාර ගත වූ දිනය', 'trim|required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('release_date', 'නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් රහිතව)', 'trim|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('release_date_rem', 'නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් සහිතව)', 'trim|min_length[10]|max_length[10]');
		//$this->form_validation->set_rules('court', 'උසාවිය', 'trim|required');
		$this->form_validation->set_rules('no_of_cases', 'විභාග වන නඩු සංඛ්‍යාව', 'trim|required');
		$this->form_validation->set_rules('inmate_cat', 'සිරකරුවන් වර්ගීකරණය', 'trim|required');
		$this->form_validation->set_rules('prison_frequency', 'සිර ගත වූ වාර ප්‍රමාණය', 'trim|required');
		//$this->form_validation->set_rules('offence', 'වරද', 'trim|required');
		$this->form_validation->set_rules('literacy', 'සාක්ෂරතා හැකියාව', 'trim|required');
		$this->form_validation->set_rules('appeal', 'ඇපිල් සිරකරුවෙක්ද?', 'trim|required');
		$this->form_validation->set_rules('special', 'විශේෂ සිරකරුවෙක්ද?', 'trim|required');
		$this->form_validation->set_rules('detention_order', 'රුඳවුම් නියෝග මත රඳවා ඇත්ද?', 'trim|required');
		$this->form_validation->set_rules('country', 'ජාතිකත්වය', 'trim|required');
		$this->form_validation->set_rules('nationality', 'ජාතිය', 'trim|required');
		$this->form_validation->set_rules('religion', 'ආගම', 'trim|required');

		if($this->form_validation->run()== false){//returns true if false
			$error= validation_errors();
			//var_dump($error);die;
			$this->session->set_flashdata('error', $error);
			//redirect(base_url() . "inmates/view_inmates");
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			//table name to save
			$table="tbl_inmate";
			//data to save in db
			$today=date('Y-m-d');
			$inmate_number=strtoupper($this->input->post("inmate_number")).'-'.$ses_institute_id;
			//change the inmate number for suspects
			$suspect=$this->input->post("suspect");
			if($suspect == '1'){
				$inmate_number=$inmate_number.'/'.date('Y');
			}

			$birthday=$this->input->post("birthday");
			$prison_date=$this->input->post("prison_date");

			$from = new DateTime($birthday);
			$to   = new DateTime($prison_date);
			$prisoned_age= $from->diff($to)->y;
			if($birthday == ''){
				$prisoned_age='';
			}
			$data=[
				"enter_by "=>$ses_login_id,
				"suspect"=>$this->input->post("suspect"),
				"inmate_number"=>$inmate_number,
				"dup_inmate_number"=>strtoupper($this->input->post("inmate_number")),
				"inmate_name"=>$this->input->post('inmate_name'),
				"birthday "=>$birthday,
				"prisoned_age"=>$prisoned_age,
				"gender"=>$this->input->post("gender"),
				"prison_date"=>$prison_date,
				"release_date_rem"=>$this->input->post("release_date_rem"),
				"release_date"=>$this->input->post("release_date"),
				//"court_id"=>$this->input->post("court"),
				"no_of_cases"=>$this->input->post("no_of_cases"),
				"inmate_category"=>$this->input->post("inmate_cat"),
				"prison_frequency"=>$this->input->post("prison_frequency"),
				//"offence_id"=>$this->input->post("offence"),
				"literacy"=>$this->input->post("literacy"),
				"appeal"=>$this->input->post("appeal"),
				"special"=>$this->input->post("special"),
				"detention_order"=>$this->input->post("detention_order"),
				"country"=>$this->input->post("country"),
				"nationality"=>$this->input->post("nationality"),
				"religion"=>$this->input->post("religion"),
				"last_update_date"=>$today
			];
			//checkling for duplicate inmate no except in the same inmate id
			$where=[
					"dup_inmate_number"=>strtoupper($this->input->post("inmate_number"))
			];
			//var_dump($where);
			$not_where=[
				'inmate_id !='=>$this->input->post("inmate_id")
			];
			//var_dump($not_where);
			$select='inmate_name';
      $key_result=$this->gen_model->get_not_where($table,$select,$where,$not_where);
			//var_dump($key_result->result());die;
			if($key_result->num_rows() == 0 || $key_result->num_rows() == 'empty'){
				$where1=[
					'inmate_id'=>$this->input->post("inmate_id")
				];
				$table2='tbl_inmate_offence';
				$table3='tbl_inmate_court';
				//if the inmate number not used by someone another, update the data
				$result=$this->gen_model->update_data($table,$data,$where1);
				$result2=$this->gen_model->delete_data($table2,$where1);
				$result3=$this->gen_model->delete_data($table3,$where1);
				$offences=$this->input->post("offence");
				foreach($offences as $row1){
					$data2[]=[
						'inmate_id'=>$this->input->post("inmate_id"),
						'offence_id'=>$row1
					];
				}
				$courts=$this->input->post("court");
				foreach($courts as $row2){
					$data3[]=[
						'inmate_id'=>$this->input->post("inmate_id"),
						'court_id'=>$row2
					];
				}
				//var_dump($data2);die;
				$result4 = $this->gen_model->insert_multi_data($data2,$table2);
				$result5 = $this->gen_model->insert_multi_data($data3,$table3);
				//var_dump($result);die;
        if ($result && $result4 && $result5) {
          $this->session->set_flashdata('success', 'රැඳවියාගේ දත්ත යාවත්කාලින කිරීම සාර්ථකයිි !');
          //redirect(base_url() . "inmates/view_inmates"); //success
		  		redirect($_SERVER['HTTP_REFERER']);
        } else {
          $this->session->set_flashdata('error', 'රැඳවියාගේ දත්ත යාවත්කාලින කිරීම අසාර්ථකයි !');
          //redirect(base_url() . "inmates/view_inmates"); // unsuccess
		  		redirect($_SERVER['HTTP_REFERER']);
        }
			}else{
				$this->session->set_flashdata('error', 'ඔබ ඇතුලත් කල සිර අංකය වෙනත් රැඳවියෙක් හට දැනටමත් ලබාදී තිබේ. එමනිසා දත්ත ඇතුලත් කිරීම අසාර්ථකයි .');
        //redirect(base_url() . "inmates/view_inmates");
				redirect($_SERVER['HTTP_REFERER']);
				redirect($referred_from, 'refresh');
			}
		}
	}

	//exchange inmate
	public function exchange_inmate(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }

		$this->form_validation->set_rules('institute', 'බන්ධනාගාර ආයතනය', 'trim|required');
		$this->form_validation->set_rules('exchange_date', 'මාරු කරන දිනය', 'trim|required|min_length[10]|max_length[10]');
		if($this->form_validation->run()== false){//returns true if false
			$error= validation_errors();
			//var_dump($error);die;
			$this->session->set_flashdata('error', $error);
			redirect(base_url() . "inmates/view_inmates");
			//redirect($_SERVER['HTTP_REFERER']);
		}else{
			$table="tbl_inmate";
			$today=date('Y-m-d');
			$where=[
					"inmate_id"=>$this->input->post('inmate_id')
			];

			$status=[
					"exchange"=>'1',
					"institute"=>$this->input->post('institute'),
					"transfered_institute"=>$ses_institute_id,
					"exchange_date"=>$this->input->post('exchange_date'),
					"last_update_date"=>$today
			];
			//var_dump($status);die;
			//load model
			$result=$this->gen_model->update_data($table,$status,$where);
			//echo json_encode($result);die;
			if ($result) {
				$this->session->set_flashdata('success', 'රැඳවියා මාරු කිරීම සාර්ථකයිි !');
				redirect(base_url() . "inmates/view_inmates"); //success
				//redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('error', 'රැඳවියා මාරු කිරීම අසාර්ථකයි !');
				redirect(base_url() . "inmates/view_inmates"); // unsuccess
				//redirect($_SERVER['HTTP_REFERER']);
			}
		}

	}

	//view active and exchanged inmates
	public function exchanged_inmates(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
			$session_data=$this->session->userdata('loggedin_user');
			//var_dump($session_data);die;
			$ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
		}

		//data to load on select options
		$table1="tbl_institute";//table name to retrieve institute data
		$table2='tbl_offence';
		$table3='tbl_court';
		$select='*';
		//load gearic modal to get data
		$result1=$this->gen_model->get_data($select,$table1);
		$data['institutes']=$result1->result();
		$result2=$this->gen_model->get_data($select,$table2);
		$data['offences']=$result2->result();
		$result3=$this->gen_model->get_data($select,$table3);
		$data['courts']=$result3->result();

		//data to load on the table
		$table3="view_inmate";
		$select2= 'inmate_id,dup_inmate_number,inmate_name,institute_name,prison_date,exchange_date,transfered_institute_name';
		$where=[
			//'institute'=> $ses_institute_id,
			'enter_by'=> $ses_login_id,
			'active_state'=> '1',
			'exchange' => '1'
		];
		$result3 = $this->gen_model->get_data($select2,$table3,$where);
		$data['inmates']=$result3->result();
		//var_dump($data);die;
		$this->load->view('data_entry/view_exchanged_inmates',$data);
	}

	//view active and exchanged inmates
	public function recieved_inmates(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
			$session_data=$this->session->userdata('loggedin_user');
			//var_dump($session_data);die;
			$ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
		}

		$select='*';
		//data to load on the table
		$table3="view_inmate";
		$select2= 'inmate_id,dup_inmate_number,inmate_name,institute_name,transfered_institute_name,exchange_date,prison_date,last_update_date';
		$where=[
			//'institute'=> $ses_institute_id,
			'institute'=> $ses_institute_id,
			'active_state'=> '1',
			'exchange' => '1'
		];
		$result3 = $this->gen_model->get_data($select2,$table3,$where);
		$data['inmates']=$result3->result();
		//var_dump($data);die;
		$this->load->view('data_entry/view_recieved_inmates',$data);
	}

	public function include_inmate(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }

		$table="tbl_inmate";
		$today=date('Y-m-d');
		$where=[
				"inmate_id"=>$this->input->post('inmate_id')
		];
		$inmate_number=strtoupper($this->input->post("inmate_number")).'-'.$ses_institute_id;
		//change the inmate number for suspects
		$suspect=$this->input->post("suspect");
		if($suspect == '1'){
			$inmate_number=$inmate_number.'/'.date('Y');
		}
		$data=[
			"enter_by "=>$ses_login_id,
			"inmate_number"=>$inmate_number,
			"exchange"=>'0',
			"last_update_date"=>$today
		];

		//var_dump($data);die;
		//load model
		$result=$this->gen_model->update_data($table,$data,$where);
		//echo json_encode($result);die;
		if ($result) {
			$this->session->set_flashdata('success', 'රැඳවියා මාරු කිරීම සාර්ථකයිි !');
			redirect(base_url() . "inmates/recieved_inmates"); //success
			//redirect($_SERVER['HTTP_REFERER']);
		} else {
			$this->session->set_flashdata('error', 'රැඳවියා මාරු කිරීම අසාර්ථකයි !');
			redirect(base_url() . "inmates/recieved_inmates"); // unsuccess
			//redirect($_SERVER['HTTP_REFERER']);
		}


	}

	//restore exchanged inmate
	public function exchange_restore_inmate(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
	    $session_data=$this->session->userdata('loggedin_user');
	    //var_dump($session_data);die;
	    $ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
	  }
		//deleteing the inmate means setting the active state to 0
		$table="tbl_inmate";
		$key['inmate_id'] = $_POST['ex_key'];
		$where=$key;
		$today=date('Y-m-d');

		$status=[
			"institute"=>$ses_institute_id,
			"exchange"=>'0',
			"last_update_date"=>$today,
			"exchange_date"=>NULL
		];
		//load model
		$result=$this->gen_model->update_data($table,$status,$where);
		//echo json_encode($result);die;
		if($result=='true'){
			echo json_encode('a');//true
		}else{
			echo json_encode($result);//false
		}
	}

	//view deleted inmates
	public function deleted_inmates(){
		if ($this->session->has_userdata('loggedin_user') == TRUE) {
			$session_data=$this->session->userdata('loggedin_user');
			//var_dump($session_data);die;
			$ses_login_id=$session_data['ses_login_id'];
			$ses_institute_id=$session_data['ses_institute_id'];
		}

		//data to load on select options
		$table="view_removed_inmate";
		$select= 'inmate_id,dup_inmate_number,inmate_name,prison_date,delete_date,release_type';
		$where=[
			//'institute'=> $ses_institute_id,
			'enter_by'=> $ses_login_id,
			'active_state'=> '0'
		];
		$result3 = $this->gen_model->get_data($select,$table,$where);
		$data['inmates']=$result3->result();
		//var_dump($data);die;
		$this->load->view('data_entry/view_deleted_inmates',$data);
	}

	//view single inmate on a modal AJAX
  public function view_single_deleted_inmate(){
    $table="view_removed_inmate";
		$table2="view_inmate_offence";
		$table3="view_inmate_court";
    $key_arr['inmate_id']= $_POST['view_key'];
		//$key_arr['inmate_id']= '1';
		//$key_arr['emp_id']= '1';
    $where=$key_arr;
    $select= '*';
    $result = $this->gen_model->get_data($select,$table,$where);
		//var_dump($result);die;
		if(!empty($result)){
      foreach ($result->result() as $row) {
        $output[1]=array(
					"inmate_id"=>$row->inmate_id,
					"inmate_number"=>$row->dup_inmate_number,
					"inmate_name"=>$row->inmate_name,
					"institute_id"=>$row->institute,
					"institute"=>$row->institute_name,
					"birthday"=>$row->birthday,
					"prisoned_age"=>$row->prisoned_age,
					"gender"=>$row->gender,
					"prison_date"=>$row->prison_date,
					"release_date_rem"=>$row->release_date_rem,
					"release_date"=>$row->release_date,
					"no_of_cases"=>$row->no_of_cases,
					"inmate_category"=>$row->inmate_category,
					"prison_frequency"=>$row->prison_frequency,
					"literacy"=>$row->literacy,
					"appeal"=>$row->appeal,
					"special"=>$row->special,
					"detention_order"=>$row->detention_order,
					"country"=>$row->country,
					"nationality"=>$row->nationality,
					"religion"=>$row->religion,
					"delete_date"=>$row->delete_date,
					"release_type"=>$row->release_type,
					"return_date"=>$row->return_date,
					"remark"=>$row->remark,
					"exchange_date"=>$row->exchange_date
        );
      }
			foreach ($result->result() as $row) {
					$inmate_id=$row->inmate_id;
      }
			$where2=[
				"inmate_id"=>$inmate_id
			];
	    $result2 = $this->gen_model->get_data($select,$table2,$where2);
			$output[2][]=[];
			foreach ($result2->result() as $row) {
					$output[2][] = [
						'offence'=>$row->offence
					];
					$output[3][] = [
						'offence_id'=>$row->offence_id
					];
					$output[4][] = [
						'io_id'=>$row->io_id
					];
      }
			//var_dump($result2->result());
			$result3 = $this->gen_model->get_data($select,$table3,$where2);
			foreach ($result3->result() as $row2) {
					$output[5][] = [
						'court'=>$row2->court_name
					];
					$output[6][] = [
						'court_id'=>$row2->court_id
					];
					$output[7][] = [
						'ic_id'=>$row2->ic_id
					];
      }
			//var_dump($output);
      echo json_encode($output);
    }else{
      echo json_encode("සමාවන්න! මෙම රැඳවියන්ගේ තොරතුරු නොමැත.");
    }
  }

	//delete inmate
	public function delete_inmate(){
		//deleteting a email is required to set the active state to 0 in tbl_inmate
		$table1="tbl_release";
		$table2="tbl_inmate";
		$where=[
				"inmate_id"=>$this->input->post('del_inmate_id')
		];
		$data=[
			"release_type_id"=>$this->input->post('release'),
			"inmate_id"=>$this->input->post('del_inmate_id'),
			"delete_date"=>$this->input->post('released_date'),
			"return_date"=>$this->input->post('return_date'),
			"remark"=>$this->input->post('remark'),
		];
		$today=date('Y-m-d');
		$status=[
				"active_state"=>'0',
				"last_update_date"=>$today,
		];
		//var_dump($status2);die;
		$result=$this->gen_model->insert_data($data,$table1);
		if($result){
			$result2=$this->gen_model->update_data($table2,$status,$where);
			if ($result2) {
				$this->session->set_flashdata('success', 'රැඳවියා ඉවත් කිරීම සාර්ථකයිි !');
				redirect(base_url() . "inmates/view_inmates"); //success
				//redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('error', 'රැඳවියා ඉවත් කිරීම අසාර්ථකයි !');
				redirect(base_url() . "inmates/view_inmates"); // unsuccess
				//redirect($_SERVER['HTTP_REFERER']);
			}
		}else {
			$this->session->set_flashdata('error', 'රැඳවියා ඉවත් කිරීම අසාර්ථකයි !');
			redirect(base_url() . "inmates/view_inmates"); // unsuccess
			//redirect($_SERVER['HTTP_REFERER']);
		}
	}

	//restore deleted inmate
	public function restore_inmate(){
		//deleteing the inmate means setting the active state to 0
		$table="tbl_inmate";
		$table2="tbl_release";
		$today=date('Y-m-d');
		$key['inmate_id'] = $_POST['restore_key'];
		$where=$key;
		$status=[
				"active_state"=>'1',
				"last_update_date"=>$today,
		];
		//load model
		$result=$this->gen_model->update_data($table,$status,$where);
		//echo json_encode($result);die;
		if($result=='true'){
			$result2=$this->gen_model->delete_data($table2,$where);
			if($result2){
				echo json_encode('a');//true
			}else{
				echo json_encode($result);//false
			}
		}else{
			echo json_encode($result);//false
		}
	}

	//live search
	public function search_inmate(){
		$table="view_inmate";
    $key_arr['dup_inmate_number']= $_POST['view_key'];
		//$key_arr['dup_inmate_number']='Y/12844/2020';
    $where=$key_arr;
    $select= 'inmate_id,dup_inmate_number,inmate_name,institute_name,prison_date,active_state';
    $result = $this->gen_model->get_data($select,$table,$where);
		//var_dump($result);die;
		if(!empty($result)){
			$output1=[];
      foreach ($result->result() as $row) {
        $output1=array(
					"inmate_number"=>$row->dup_inmate_number,
					"inmate_name"=>$row->inmate_name,
					"institute"=>$row->institute_name,
					"prison_date"=>$row->prison_date,
					"active_state"=>$row->active_state
        );
				$state=$row->active_state;
				$inmate_id=$row->inmate_id;
      }
			$output2=[];
			if($state == '0'){
				$table="view_release";
				$select= 'delete_date,release_type';
				$where=['inmate_id'=>$inmate_id];
				$result2 = $this->gen_model->get_data($select,$table,$where);

				foreach ($result2->result() as $row) {
	        $output2=array(
						"delete_date"=>$row->delete_date,
						"release_type"=>$row->release_type
	        );
	      }
			}

			$output=array_merge($output1,$output2);
			//var_dump($output);die;
      echo json_encode($output);
    }else{
      echo json_encode('false');
    }
	}

}
