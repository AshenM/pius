<?php

class Gen_model extends CI_Model{

  public function key_search($key,$table){
    $query=$this->db->get_where($table,$key);
    //var_dump($query->result());die;
    return $query;
  }

  //all the data retrive genaric function
  public function get_data($select, $table, $where = "") {
    //check for exixstence of where close
    if ($where == "") {
        $this->db->select($select)->from($table);
    } else {
        $this->db->select($select)->from($table)->where($where);
    }
    //select query
    $query = $this->db->get();
    return $query;
  }

  //data insert genaric function
  public function insert_data_return_id($data,$table){
      //insert query
      $result = $this->db->insert($table, $data);
      $last_id=$this->db->insert_id();
      return $last_id;
  }

  //data insert genaric function
  public function insert_data($data,$table){
      //insert query
      $result = $this->db->insert($table, $data);
      return $result;
  }

  //data update genaric function
  //sometimes this uses to delete and restore front end values by updating a active state to 0 or 1
  public function update_data($table,$data,$where = "") {
      $this->db->where($where);
      //data update query
      $result=$this->db->update($table, $data);
      return $result;
  }

  //get data not where
  public function get_not_where($table,$select,$where,$not_where){
      $this->db->select($select)->from($table);
      $this->db->where($where);
      $this->db->where($not_where);
      $query = $this->db->get();
      return $query;
  }

  //sum of the data
  public function sum_data($select, $table, $where = "") {
    //check for exixstence of where close
    if ($where == "") {
        $this->db->select_sum($select)->from($table);
    } else {
        $this->db->select_sum($select)->from($table)->where($where);
    }
    //select query
    $query = $this->db->get();
    return $query;
  }

  //get maximun value
  public function get_max($select,$max, $table, $where = "") {
    //check for exixstence of where close
    if ($where == "") {
      $this->db->select($select);
      $this->db->select_max($max)->from($table);
    } else {
      $this->db->select($select);
      $this->db->select_max($max)->from($table)->where($where);
    }
    //select query
    $query = $this->db->get();
    return $query;
  }

  //manual query function
  public function query_get($query){
    //batch insert query
    $result = $this->db->query($query);
    return $result;
  }

  //multiple data insert genaric function
   public function insert_multi_data($data,$table){
     //batch insert query
     $result = $this->db->insert_batch($table, $data);
     return $result;
   }

   // data delete genaric function
  public function delete_data($table, $where) {
    //data delete query
    $result=$this->db->delete($table, $where);
    return $result;
  }

  // get data from a specific query
  public function gen_query($query) {
   $result=$this->db->query($query);
   return $result;
  }


}
