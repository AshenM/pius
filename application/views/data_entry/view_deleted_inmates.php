<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Data Entry'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>නිදහස් කල, මරණයට පත් වූ සහ පැනගිය රැඳවියන්ගේ තොරතුරු</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- data table -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- date picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- bootstrap validator -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('common/header.php'); ?>
      <?php $this->load->view('common/left_menu.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header body_text"><!-- content-header -->
          <h1 style='text-align:center'>නිදහස් කල, මරණයට පත් වූ සහ පැනගිය රැඳවියන්ගේ තොරතුරු</h1>
        </section><!-- /.content-header -->

        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->
            <div class='col-md-12 col-xs-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-user-times"> </i> රැඳවියන්ගේ තොරතුරු </h3>
                </div>
                <div class="box-body table-responsive">
                  <table id="tbl_inmates" class="table table-bordered table-striped ">
                    <thead>
                      <tr>
                        <th>සිර අංකය</th>
                        <th>නම</th>
                        <th>බන්ධනාගාර ගත වූ දිනය</th>
                        <th>නිදහස් කල/මරණයට පත් වූ/පැනගිය දිනය</th>
                        <th>නිදහස් වූ ආකාරය</th>
                        <th width="100px">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($inmates as $row): ?>
                          <tr>
                            <td><?= $row->dup_inmate_number ?></td>
                            <td><?= $row->inmate_name ?></td>
                            <td><?= $row->prison_date ?></td>
                            <td><?= $row->delete_date ?></td>
                            <td><?= $row->release_type ?></td>
                            <td><button type='button' id='<?= $row->inmate_id ?>' class='view_btn btn btn-primary btn-sm' data-toggle='modal' data-target='#view_removed_inmate'><span class='fa fa-eye'></span></button>
                            &nbsp;<button type='button' id='<?= $row->inmate_id ?>' class='exchange_btn btn btn-danger btn-sm' data-toggle="tooltip" data-placement="top" title="ඉවත් කල රැඳවියා නැවත පිහිටුවන්න"><span class='fa fa-undo'></span></button>
                            </td>
                          </tr>
                        <?php endforeach;  ?>
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->
      <!-- view,edit modals -->
      <?php $this->view('modals/data_entry/view_removed_inmate_modal.php'); ?>

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?><!-- DataTables -->
    <script src="<?php echo base_url(); ?>theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap validator -->
    <script src="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/js/bootstrapValidator.js"></script>
    <script>
      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
      });

      //data table load
      $(document).ready(function(){
        $('#tbl_inmates').DataTable({
        });
      });

      //tooltip
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
      });

      //view single inmate
      $(document).on('click', '.view_btn', function () {
        var view_key = $(this).attr("id");
        //console.log(view_key);
        $.ajax({
          url: "<?php echo base_url(); ?>inmates/view_single_deleted_inmate",
          method: "POST",
          dataType: 'JSON',
          data: {view_key: view_key},
          success: function (data) {
            console.log(data[5]);
            $('#view_inmate_number').text(data[1].inmate_number);
            $('#view_inmate_name').text(data[1].inmate_name);
            $('#view_institute').text(data[1].institute);
            if(data[1].suspect=='1'){
              $('#view_suspect').text('ඔව්');
            }else {
              $('#view_suspect').text('නැත');
            }
            if(data[1].birthday=='0000-00-00'){
              $('#view_birthday').text('-');
            }else {
              $('#view_birthday').text(data[1].birthday);
            }
            $('#view_prisoned_age').text(data[1].prisoned_age);
            $('#view_gender').text(data[1].gender);
            $('#view_prison_date').text(data[1].prison_date);
            if(data[1].release_date=='0000-00-00'){
              $('#view_release_date').text('-');
            }else {
              $('#view_release_date').text(data[1].release_date);
            }
            if(data[1].release_date_rem=='0000-00-00'){
              $('#view_release_date_rem').text('-');
            }else {
              $('#view_release_date_rem').text(data[1].release_date_rem);
            }
            //$('#view_court').text(data[1].court_name);
            $('#view_no_of_cases').text(data[1].no_of_cases);
            $('#view_inmate_category').text(data[1].inmate_category);
            $('#view_prison_frequency').text(data[1].prison_frequency);
            //$('#view_offence').text(data[2][].offence);
            $('#view_literacy').text(data[1].literacy);
            $('#view_appeal').text(data[1].appeal);
            $('#view_special').text(data[1].special);
            $('#view_detention_order').text(data[1].detention_order);
            $('#view_country').text(data[1].country);
            $('#view_nationality').text(data[1].nationality);
            $('#view_religion').text(data[1].religion);
            $('#delete_date').text(data[1].delete_date);
            $('#release_type').text(data[1].release_type);
            $('#return_date').text(data[1].return_date);
            $('#remark').text(data[1].remark);

             $("#view_court").empty();
            $.each(data[5], function (i,currData2) {
                $.each(currData2, function (x,y) {
                  document.getElementById("view_court").innerHTML  += "<li>" +y + "</li>";
                });
            });

            $("#view_offence").empty();
            $.each(data[2], function (i,currData) {
                $.each(currData, function (x,y) {
                  document.getElementById("view_offence").innerHTML  += "<li>" +y + "</li>";
                });
            });

          }
        });
      });

      //exchange single inmate
      $(document).on('click', '.exchange_btn', function () {
        var restore_key = $(this).attr("id");
        // console.log(del_key);
        Swal.fire({
          title: "තහවුරු කරන්න!",
          text: "රැඳවියා නැවත පිහිටවූ පසු, මෙම වගුවෙන් ඉවත්වී නැවතත් රැඳවි වගුවේ පෙන්වයි",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'එපා',
          confirmButtonText: 'නැවත පිහිටුවන්න!'
        })
        .then((result) => {
          if (result.value) {
            $.ajax({
              url: "<?php echo base_url(); ?>inmates/restore_inmate",
              method: "POST",
              dataType: 'JSON',
              data: {restore_key: restore_key},
              success: function (data) {
                //console.log(data);
                if(data == 'a'){
                  Swal.fire('සාර්ථකයි!','රැඳවියා නැවත පිහිටුවන ලදී.','success').then((refresh)=>{
                    if(refresh){
                        location.reload();
                    }
                  });
                }else{
                  Swal.fire('Something went wrong!','Error Message ('+data+')','error').then((refresh)=>{
                    if(refresh){
                        location.reload();
                    }
                  });
                }

              }
            });
          }
        });
      });

      $(document).ready(function () {
        $('#l_inm').addClass('active');
        $('#l_inm5').addClass('active');
      });
    </script>

  </body>
</html>
