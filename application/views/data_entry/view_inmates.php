<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Data Entry'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>රැඳවියන්ගේ තොරතුරු</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- data table -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- date picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- bootstrap validator -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/select2/dist/css/select2.min.css">
    <style>
      /* select 2 style overide  */
      .select2-container--default .select2-selection--single {
        background-color: #ccc;
        border: 1px solid #999;
        border-radius: 0;
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        color: #555;
        border: 1px solid #999;
      }
      .select2-container--default .select2-selection--multiple {
        background-color: #ccc;
        border: 1px solid #999;
        border-radius: 0;
        display: block;
        width: 220px;
        padding: 2px 12px;
        color: #555;
        border: 1px solid #999;
      }
      .select2-container--default .select2-selection--multiple .select2-selection__choice {
        border-color: #367fa9;
        padding: 1px 10px;
        width: auto;
        color: #111;
      }
    </style>
  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('common/header.php'); ?>
      <?php $this->load->view('common/left_menu.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header body_text"><!-- content-header -->
          <h1 style='text-align:center'>රැඳවියන්ගේ තොරතුරු</h1>
        </section><!-- /.content-header -->

        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->
            <div class='col-md-12 col-xs-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-users"> </i> රැඳවියන්ගේ තොරතුරු </h3>
                </div>
                <div class="box-body table-responsive">
                  <table id="tbl_inmates" class="table table-bordered table-striped ">
                    <thead>
                      <tr>
                        <th>සිර අංකය</th>
                        <th>නම</th>
                        <th>බන්ධනාගාර ගත වූ දිනය</th>
                        <th>සිරකරුවන් වර්ගීකරණය</th>
                        <th>බන්ධනාගාර ආයතනය</th>
                        <th width="100px">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($inmates as $row): ?>
                          <tr>
                            <td><?= $row->dup_inmate_number ?></td>
                            <td><?= $row->inmate_name ?></td>
                            <td><?= $row->prison_date ?></td>
                            <td><?= $row->inmate_category ?></td>
                            <td><?= $row->institute_name ?></td>
                            <td><button type='button' id='<?= $row->inmate_id ?>' class='view_btn btn btn-primary btn-sm' data-toggle='modal' data-target='#view_inmate'><span class='fa fa-eye'></span></button>
                            &nbsp;<button type='button' id='<?= $row->inmate_id ?>' class='edit_btn btn btn-info btn-sm' data-toggle='modal' data-target='#edit_inmate'><span class='fa fa-edit'></span></button>
                            &nbsp;<button type='button' id='<?= $row->inmate_id ?>' class='delete_btn btn btn-success btn-sm' data-toggle='modal' data-target='#delete_inmate'><span class='fa fa-home'></span></button>
                          </td>

                          </tr>
                        <?php endforeach;  ?>
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->
      <!-- view,edit modals -->
      <?php $this->view('modals/data_entry/view_inmate_modal.php'); ?>
      <?php $this->view('modals/data_entry/edit_inmate_modal.php'); ?>
      <?php $this->view('modals/data_entry/delete_inmate_modal.php'); ?>

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?><!-- DataTables -->
    <script src="<?php echo base_url(); ?>theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap validator -->
    <script src="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/js/bootstrapValidator.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url();?>theme/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script>
      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
      });

      //data table load
      $(document).ready(function(){
        $('#tbl_inmates').DataTable({
          deferRender: true,//croller
          scroller: true//croller
        });
      });

      //select the current prison institute based on session data
       $(document).ready(function(){
         //$('#institute [value="'+<?php echo $ses_institute_id?>+'"]').attr('selected', 'selected').change();

         //Initialize Select2 Elements
        $('.select2').select2();

        //remove the current institute from transfer institute list
        $("#institute option[value='"+<?php echo $ses_institute_id?>+"']").remove();
       });

       //appointment Date picker
       $(function () {
         //Date picker
         $('#birthday').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           endDate: '-15y',
           todayHighlight:'true'
         });
         $('#prison_date').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           defaultViewDate: 'today',
           endDate: '0d',
           todayHighlight:'true'
         });
         $('#release_date').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           startDate: '+1d',
           todayHighlight:'true'
         });
         $('#release_date_rem').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           startDate: '+1d',
           todayHighlight:'true'
         });
         $('#return_date').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           startDate: '+1d',
           todayHighlight:'true'
         });
         $('#released_date').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           endDate: '+2d',
           todayHighlight:'true'
         });
         $('#exchange_date').datepicker({
           autoclose: true,
           format: 'yyyy-mm-dd',
           endDate: '+2d',
           todayHighlight:'true'
         });
       });

      //show hide fields when check the checkbox
      $(document).ready(function (){
        $('.exchange_show').hide();
        $("#exchange").change(function() {
            if(this.checked) {
              $('.exchange_hide').hide();
              $('.exchange_show').show();
              $('#update_inmates').attr('action', '<?php echo base_url(); ?>inmates/exchange_inmate');
            }else{
              $('.exchange_hide').show();
              $('.exchange_show').hide();
              $('#update_inmates').attr('action', '<?php echo base_url(); ?>inmates/update_inmates');
            }
        });
      });

    //  bootstrapValidator
    // $('#remove_inmates').bootstrapValidator({
    //     message: 'This value is not valid',
    //     fields: {
    //       release: {
    //         message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
    //         validators: {
    //           notEmpty: {
    //             message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර නිදහස් වන ආකාරය ඇතුලත් කරන්න'
    //           }
    //         }
    //       },
    //       return_date: {
    //         message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
    //         validators: {
    //           date: {
    //             format: 'YYYY-MM-DD',
    //             message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ නැවත පැමිණෙන දිනය වලංගු නොවේ'
    //           }
    //         }
    //       },
    //       released_date: {
    //         message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
    //         validators: {
    //           notEmpty: {
    //             message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර නිදහස්වූ දිනය / මරණයට පත්වූ දිනය ඇතුලත් කරන්න'
    //           },
    //           date: {
    //             format: 'YYYY-MM-DD',
    //             message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ නිදහස්වූ දිනය / මරණයට පත්වූ දිනය වලංගු නොවේ'
    //           }
    //         }
    //       }
    //     }
    // });

      // bootstrapValidator
      $('#update_inmates').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
          institute: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර බන්ධනාගාර ආයතනයක් තෝරන්න'
              }
            }
          },
          inmate_number: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සිර අංකයක් ඇතුලත් කරන්න'
              },
              regexp: {
                regexp: /^[A-Z a-z]{1}[\/][0-9]{5}[\/][0-9]{4}$/,
                message: '<i class="fa fa-times-circle">&nbsp;</i>සිර අංකය ඉංග්‍රීසි අකුරකින් ආරම්භ වී / ලකුණට පසු ඉලක්කම් 5ක් ද නැවත / ලකුණද ඉන්පසු ඉලක්කම් 4ක වර්ෂයක් තිබිය යුතුය <br/><i class="fa fa-circle">&nbsp;</i>උදා:- X/00001/2019'

              }
            }
          },
          inmate_name: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර නම ඇතුලත් කරන්න'
              },
              regexp: {
                regexp: /^(?![\s.]+$)[a-zA-Z\s]*$/,
                message: '<i class="fa fa-times-circle">&nbsp;</i>නමෙහි අකුරු පමණක් තිබිය යුතුයි'
              }
            }
          },
          birthday: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ උපන් දිනය වලංගු නොවේ'
              }
            }
          },
          gender: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ස්ත්‍රී පුරුෂ භාවය ඇතුලත් කරන්න'
              }
            }
          },
          prison_date: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර බන්ධනාගාර ගත වූ දිනය ඇතුලත් කරන්න'
              },
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ බන්ධනාගාර ගත වූ දිනය වලංගු නොවේ'
              }
            }
          },
          release_date: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ නිදහස ලැබීමට නියමිත දිනය වලංගු නොව'
              }
            }
          },
          release_date_rem: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ නිදහස ලැබීමට නියමිත දිනය වලංගු නොව'
              }
            }
          },
          court: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර උසාවිය ඇතුලත් කරන්න'
              }
            }
          },
          no_of_cases: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර විභාග වන නඩු සංඛ්‍යාව ඇතුලත් කරන්න'
              },
              regexp: {
                regexp: /^[0-9]*$/,
                message: '<i class="fa fa-times-circle">&nbsp;</i>විභාග වන නඩු සංඛ්‍යාව ඉලක්කම් පමණක් විය යුතුයිි'
              }
            }
          },
          inmate_cat: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සිරකරුවන් වර්ගීකරණය ඇතුලත් කරන්න'
              }
            }
          },
          prison_frequency: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සිර ගත වූ වාර ප්‍රමාණය ඇතුලත් කරන්න'
              }
            }
          },
          offence: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර වරදක් තෝරන්න'
              }
            }
          },
          appeal: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ඇපිල් සිරකරුවෙක්ද යනවග ඇතුලත් කරන්න'
              }
            }
          },
          special: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර විශේෂ සිරකරුවෙක්ද යනවග ඇතුලත් කරන්න'
              }
            }
          },
          detention_order: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර රැඳවුම් නියෝග මත රඳවා ඇත්ද යනවග තෝරන්න'
              }
            }
          },
          literacy: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සාක්ෂරතා හැකියාව ඇතුලත් කරන්න'
              }
            }
          },
          country: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ජාතිකත්වය ඇතුලත් කරන්න'
              }
            }
          },
          nationality: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ජාතිය ඇතුලත් කරන්න'
              }
            }
          },
          religion: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ආගම් ඇතුලත් කරන්න'
              }
            }
          }
        }
    });

    //view single inmate
    $(document).on('click', '.view_btn', function () {
      var view_key = $(this).attr("id");
      //console.log(view_key);
      $.ajax({
        url: "<?php echo base_url(); ?>inmates/view_single_inmate",
        method: "POST",
        dataType: 'JSON',
        data: {view_key: view_key},
        success: function (data) {
          console.log(data[5]);
          $('#view_inmate_number').text(data[1].inmate_number);
          $('#view_inmate_name').text(data[1].inmate_name);
          $('#view_institute').text(data[1].institute);
          if(data[1].suspect=='1'){
            $('#view_suspect').text('ඔව්');
          }else {
            $('#view_suspect').text('නැත');
          }
          if(data[1].birthday=='0000-00-00'){
            $('#view_birthday').text('-');
          }else {
            $('#view_birthday').text(data[1].birthday);
          }
          $('#view_prisoned_age').text(data[1].prisoned_age);
          $('#view_gender').text(data[1].gender);
          $('#view_prison_date').text(data[1].prison_date);
          if(data[1].release_date=='0000-00-00'){
            $('#view_release_date').text('-');
          }else {
            $('#view_release_date').text(data[1].release_date);
          }
          if(data[1].release_date_rem=='0000-00-00'){
            $('#view_release_date_rem').text('-');
          }else {
            $('#view_release_date_rem').text(data[1].release_date_rem);
          }
          //$('#view_court').text(data[1].court_name);
          $('#view_no_of_cases').text(data[1].no_of_cases);
          $('#view_inmate_category').text(data[1].inmate_category);
          $('#view_prison_frequency').text(data[1].prison_frequency);
          //$('#view_offence').text(data[2][].offence);
          $('#view_literacy').text(data[1].literacy);
          $('#view_appeal').text(data[1].appeal);
          $('#view_special').text(data[1].special);
          $('#view_detention_order').text(data[1].detention_order);
          $('#view_country').text(data[1].country);
          $('#view_nationality').text(data[1].nationality);
          $('#view_religion').text(data[1].religion);

           $("#view_court").empty();
          $.each(data[5], function (i,currData2) {
              $.each(currData2, function (x,y) {
                document.getElementById("view_court").innerHTML  += "<li>" +y + "</li>";
              });
          });

          $("#view_offence").empty();
          $.each(data[2], function (i,currData) {
              $.each(currData, function (x,y) {
                document.getElementById("view_offence").innerHTML  += "<li>" +y + "</li>";
              });
          });

        }
      });
    });

    //load data to edit single inmate modal
    $(document).on('click', '.edit_btn', function () {
      var view_key = $(this).attr("id");
      //console.log(view_key);
      $.ajax({
        url: "<?php echo base_url(); ?>inmates/view_single_inmate",
        method: "POST",
        dataType: 'JSON',
        data: {view_key: view_key},
        success: function (data) {
          //console.log(data.inmate_category);
          $('#inmate_id').val(data[1].inmate_id);
          $('#inmate_number').val(data[1].inmate_number);
          $('#inmate_name').val(data[1].inmate_name);
          //$('#institute [value="'+data[1].institute_id+'"]').attr('selected', 'selected').change();
          if(data[1].suspect=='1'){
            $("#suspect").prop("checked", true);
          }if(data[1].suspect !='1'){
            $("#suspect").prop("checked", false);
          }
          $('#birthday').val(data[1].birthday);
          if(data[1].birthday=='0000-00-00'){
            $('#birthday').val('');
          }else {
            $('#birthday').val(data[1].birthday);
          }
          $('#prisoned_age').val(data[1].prisoned_age);
          $('#gender').val(data[1].gender);
          if(data[1].gender=='පුරුෂ'){
            $("#gender_male").prop("checked", true);
          }else if(data[1].gender =='ස්ත්‍රී'){
            $("#gender_female").prop("checked", true);
          }
          $('#prison_date').val(data[1].prison_date);
          if(data[1].release_date_rem=='0000-00-00'){
            $('#release_date_rem').val('');
          }else {
            $('#release_date_rem').val(data[1].release_date_rem);
          }
          if(data[1].release_date=='0000-00-00'){
            $('#release_date').val('');
          }else {
            $('#release_date').val(data[1].release_date);
          }
          //$('#court [value="'+data[1].court_id+'"]').attr('selected', 'selected').change();
          $('#no_of_cases').val(data[1].no_of_cases);
          if(data[1].inmate_category=='සාමාන්‍ය'){
            $("#inmate_cat_n").prop("checked", true);
          }else if(data[1].inmate_category =='ජීවිතාන්ත'){
            $("#inmate_cat_l").prop("checked", true);
          }else if(data[1].inmate_category =='ම.ද.නී'){
            $("#inmate_cat_d").prop("checked", true);
          }
          if(data[1].prison_frequency=='FO'){
            $("#prison_frequency_fo").prop("checked", true);
          }else if(data[1].prison_frequency =='RC'){
            $("#prison_frequency_rc").prop("checked", true);
          }else if(data[1].prison_frequency =='RRC'){
            $("#prison_frequency_rrc").prop("checked", true);
          }
          $('#offence [value="'+data[2].offence_id+'"]').attr('selected', 'selected').change();
          $('#literacy').val(data[1].literacy);
          if(data[1].literacy=='ඇත'){
            $("#literacy_y").prop("checked", true);
          }else if(data[1].literacy =='නැත'){
            $("#literacy_n").prop("checked", true);
          }
          if(data[1].appeal=='ඇපිල් කර ඇත'){
            $("#appeal_y").prop("checked", true);
          }else if(data[1].appeal =='නැත'){
            $("#appeal_n").prop("checked", true);
          }
          if(data[1].special=='විශේෂ'){
            $("#special_s").prop("checked", true);
          }else if(data[1].special =='සාමාන්‍ය'){
            $("#special_n").prop("checked", true);
          }
          $('#detention_order').val(data[1].detention_order);
          $('#country').val(data[1].country);
          if(data[1].country=='ශ්‍රී ලාංකික'){
            $("#country_sl").prop("checked", true);
          }else if(data[1].country =='වෙනත්'){
            $("#country_ot").prop("checked", true);
          }
          $('#nationality [value="'+data[1].nationality+'"]').attr('selected', 'selected').change();
          $('#religion [value="'+data[1].religion+'"]').attr('selected', 'selected').change();
          if(data[1].gender=='Female'){
            $("#edit_gender_female").prop("checked", true);
          }else{
            $("#edit_gender_male").prop("checked", true);
          }

          //deselect selected values first
          $('select#offence option').removeAttr("selected");
          $('select#court option').removeAttr("selected");

          $.each(data[6], function (i,currData4) {
              $.each(currData4, function (x,y) {
                $('#court [value="'+y+'"]').attr('selected', 'selected').change();
              });
          });
          //then reseect multiple values
          //$('#offence').removeAttr("selected");
          $('#offence').attr('selected', 'selected').change();
          $.each(data[3], function (i,currData3) {
              $.each(currData3, function (x,y) {
                //console.log(y);
                $('#offence [value="'+y+'"]').attr('selected', 'selected').change();
                //$("#offence").val( [y] );
                //document.getElementById("view_offence").innerHTML  += "<li>" +y + "</li>";
              });
          });
        }
      });
    });

    //load data to delete single inmate modal
    $(document).on('click', '.delete_btn', function () {
      var del_key = $(this).attr("id");
      $('#del_inmate_id').val(del_key);
      //console.log(del_key);
    });

    $(document).ready(function () {
      $('#l_inm').addClass('active');
      $('#l_inm2').addClass('active');
    });
    </script>

  </body>
</html>
