<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Data Entry'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>රැඳවියන්ගේ තොරතුරු ඇතුලත් කිරීමේ පෝරමය</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- date picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- bootstrap validator -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/select2/dist/css/select2.min.css">
    <style>
      /* select 2 style overide  */
      .select2-container--default .select2-selection--single {
        background-color: #ccc;
        border: 1px solid #999;
        border-radius: 0;
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        color: #555;
        border: 1px solid #999;
      }
      .select2-container--default .select2-selection--multiple {
        background-color: #ccc;
        border: 1px solid #999;
        border-radius: 0;
        display: block;
        width: 100%;
        padding: 2px 12px;
        color: #555;
        border: 1px solid #999;
      }
      .select2-container--default .select2-selection--multiple .select2-selection__choice {
        border-color: #367fa9;
        padding: 1px 10px;
        color: #111;
      }
    </style>
  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('common/header.php'); ?>
      <?php $this->load->view('common/left_menu.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header body_text"><!-- content-header -->
          <h1 style='text-align:center'>රැඳවියන්ගේ තොරතුරු ඇතුලත් කිරීමේ පෝරමය</h1>
        </section><!-- /.content-header -->

        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->
            <div class='col-md-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-user-plus"> </i> නව රැඳවියන් </h3>
                </div>
                  <form class="form-horizontal" id="register" action="<?php echo base_url(); ?>inmates/insert_inmates" method="POST" role="form" name="register" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="col-md-6">

                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >බන්ධනාගාර ආයතනය *:</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="hidden" class="form-control" name="institute" value='<?php echo $ses_institute_id; ?>' id="institute" required>
                            <label class="" ><?php echo $ses_institute_name; ?></label>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >සිර අංකය *:</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control" name="inmate_number" id="inmate_number" required>
                          </div>
                        </div>
                        <!-- <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >සැක :</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="checkbox" class="" name="suspect" id="suspect" value='1'>
                          </div>
                        </div> -->
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >නම *:</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control" name="inmate_name" id="inmate_name" required>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >උපන් දිනය :</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control date" name="birthday" id="birthday" data-provide="datepicker" placeholder="Year/Month/Date">
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >ස්ත්‍රී පුරුෂ භාවය *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="පුරුෂ" name="gender" id="gender" checked> <lable>පුරුෂ &nbsp;&nbsp;</lable>
                              <input type="radio" value="ස්ත්‍රී" name="gender" id="gender"> <lable>ස්ත්‍රී</lable>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >බන්ධනාගාර ගත වූ දිනය *:</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control date" name="prison_date" id="prison_date" data-provide="datepicker" placeholder="Year/Month/Date" required>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් සහිතව):</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control date" name="release_date_rem" id="release_date_rem" data-provide="datepicker" placeholder="Year/Month/Date">
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් රහිතව):</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control date" name="release_date" id="release_date" data-provide="datepicker" placeholder="Year/Month/Date">
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >උසාවිය *:</label>
                          </div>
                          <div class='col-md-8 '>
                            <!-- <input type="text" class="form-control" name="court" id="court" required> -->
                            <select class="form-control select2 type_select" multiple name="court[]" id="court" required>
                              <option value=''></option>
                              <?php foreach($courts as $row): ?>
                                <option value="<?php echo $row->court_id; ?>" ><?php echo $row->court_name; ?></option>
                              <?php endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >විභාග වන නඩු සංඛ්‍යාව *:</label>
                          </div>
                          <div class='col-md-8'>
                            <input type="text" class="form-control" name="no_of_cases" id="no_of_cases" required>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >සිරකරුවන් වර්ගීකරණය *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="සාමාන්‍ය" name="inmate_cat" id="inmate_cat" checked> <lable>සාමාන්‍ය &nbsp;&nbsp;</lable>
                              <input type="radio" value="ජීවිතාන්ත" name="inmate_cat" id="inmate_cat"> <lable>ජීවිතාන්ත &nbsp;&nbsp;</lable>
                              <input type="radio" value="ම.ද.නී" name="inmate_cat" id="inmate_cat"> <lable>ම.ද.නී</lable>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >සිර ගත වූ වාර ප්‍රමාණය *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="FO" name="prison_frequency" id="prison_frequency" checked> <lable>FO &nbsp;&nbsp;</lable>
                              <input type="radio" value="RC" name="prison_frequency" id="prison_frequency"> <lable>RC &nbsp;&nbsp;</lable>
                              <input type="radio" value="RRC" name="prison_frequency" id="prison_frequency"> <lable>RRC</lable>
                            </span>
                          </div>
                        </div>
                      </div>

                      <!-- right side -->
                      <div class="col-md-6">

                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >වරද *:</label>
                          </div>
                          <div class='col-md-8'>
                            <select class="form-control select2" name="offence[]" multiple='multiple' id="offence" required>
                              <option value=''></option>
                              <?php foreach($offences as $row): ?>
                                <option value="<?php echo $row->offence_id; ?>" ><?php echo $row->offence; ?> </option>
                              <?php endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >ඇපිල් සිරකරුවෙක්ද? *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="නැත" name="appeal" id="appeal" checked> <lable>නැත &nbsp;&nbsp;</lable>
                              <input type="radio" value="ඇපිල් කර ඇත" name="appeal" id="appeal" > <lable>ඇපිල් කර ඇත </lable>
                            </span>
                          </div>
                        </div>

                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >විශේෂ සිරකරුවෙක්ද? *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="සාමාන්‍ය" name="special" id="special" checked> <lable>සාමාන්‍ය &nbsp;&nbsp;</lable>
                              <input type="radio" value="විශේෂ" name="special" id="special" > <lable>විශේෂ </lable>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >රැඳවුම් නියෝග මත රඳවා ඇත්ද? *:</label>
                          </div>
                          <div class='col-md-8'>
                            <select class="form-control" name="detention_order" id="detention_order" required>
                              <option value='නැත'>නැත</option>
                              <option value='JVP'>JVP</option>
                              <option value='LTTE'>LTTE</option>
                              <option value='NTJ'>NTJ</option>
                              <option value='වෙනත්'>වෙනත්</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >සාක්ෂරතා හැකියාව *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="ඇත" name="literacy" id="literacy" checked> <lable>ඇත &nbsp;&nbsp;</lable>
                              <input type="radio" value="නැත" name="literacy" id="literacy"> <lable>නැත</lable>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >ජාතිකත්වය *:</label>
                          </div>
                          <div class='col-md-8' >
                            <span>
                              <input type="radio" value="ශ්‍රී ලාංකික" name="country" id="country" checked> <lable>ශ්‍රී ලාංකික &nbsp;&nbsp;</lable>
                              <input type="radio" value="වෙනත්" name="country" id="country"> <lable>වෙනත්</lable>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >ජාතිය *:</label>
                          </div>
                          <div class='col-md-8'>
                            <select class="form-control" name="nationality" id="nationality" required>
                              <option value=''></option>
                              <option value='සිංහල'>සිංහල</option>
                              <option value='දෙමළ'>දෙමළ</option>
                              <option value='මුස්ලිම්'>මුස්ලිම්</option>
                              <option value='බර්ගර්'>බර්ගර්</option>
                              <option value='මැලේ'>මැලේ</option>
                              <option value='වෙනත්'>වෙනත්</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-12 form-group">
                          <div class="col-md-4">
                            <label class="control-label" >ආගම *:</label>
                          </div>
                          <div class='col-md-8'>
                            <select class="form-control" name="religion" id="religion" required>
                              <option value=''></option>
                              <option value='බෞද්ධ'>බෞද්ධ</option>
                              <option value='කතෝලික'>කතෝලික</option>
                              <option value='හින්දු'>හින්දු</option>
                              <option value='ඉස්ලාම්'>ඉස්ලාම්</option>
                              <option value='වෙනත්'>වෙනත්</option>
                            </select>
                          </div>
                        </div>

                      </div>



                    </div>
                            <!-- /.box-body -->
                    <div class="box-footer">
                      <button type="reset" class="btn btn-default">Cancel</button>
                      <button type="submit" class="btn btn-primary pull-right">Save </button>
                    </div>
                    <!-- /.box-footer -->
                  </form>

                </div>
            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?>

    <!-- datepicker -->
    <script src="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap validator -->
    <script src="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/js/bootstrapValidator.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url();?>theme/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script>
      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
      });

      //appointment Date picker
      $(function () {
        //Date picker
        $('#birthday').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          endDate: '-15y',
          todayHighlight:'true'
        });
        $('#prison_date').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          defaultViewDate: 'today',
          endDate: '0d',
          todayHighlight:'true'
        });
        $('#release_date').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          startDate: '+1d',
          todayHighlight:'true'
        });
        $('#release_date_rem').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          startDate: '+1d',
          todayHighlight:'true'
        });

      });


      //select the current prison institute based on session data
       $(document).ready(function(){
      //   $('#institute [value="'+<?php //echo $ses_institute_id?>+'"]').attr('selected', 'selected').change();
      //Initialize Select2 Elements
        $('.select2').select2();
       });



      // bootstrapValidator
      $('#register').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
          institute: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර බන්ධනාගාර ආයතනයක් තෝරන්න'
              }
            }
          },
          inmate_number: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සිර අංකයක් ඇතුලත් කරන්න'
              },
              regexp: {
                regexp: /^[A-Z a-z]{1}[\/][0-9]{5}[\/][0-9]{4}$/,
                message: '<i class="fa fa-times-circle">&nbsp;</i>සිර අංකය ඉංග්‍රීසි අකුරකින් ආරම්භ වී / ලකුණට පසු ඉලක්කම් 5ක් ද නැවත / ලකුණද ඉන්පසු ඉලක්කම් 4ක වර්ෂයක් තිබිය යුතුය <br/><i class="fa fa-circle">&nbsp;</i>උදා:- X/00001/2019'

              }
            }
          },
          inmate_name: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර නම ඇතුලත් කරන්න'
              },
              regexp: {
                regexp: /^(?![\s.]+$)[a-zA-Z.\s]*$/,
                message: '<i class="fa fa-times-circle">&nbsp;</i>නමෙහි අකුරු පමණක් තිබිය යුතුයි'
              }
            }
          },
          birthday: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ උපන් දිනය වලංගු නොවේ'
              }
            }
          },
          gender: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ස්ත්‍රී පුරුෂ භාවය ඇතුලත් කරන්න'
              }
            }
          },
          prison_date: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර බන්ධනාගාර ගත වූ දිනය ඇතුලත් කරන්න'
              },
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ බන්ධනාගාර ගත වූ දිනය වලංගු නොවේ'
              }
            }
          },
          release_date: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ නිදහස ලැබීමට නියමිත දිනය වලංගු නොව'
              }
            }
          },
          release_date_rem: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              date: {
                format: 'YYYY-MM-DD',
                message: '<i class="fa fa-times-circle">&nbsp;</i>ඔබ යෙදූ නිදහස ලැබීමට නියමිත දිනය වලංගු නොව'
              }
            }
          },
          court: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර උසාවිය ඇතුලත් කරන්න'
              }
            }
          },
          no_of_cases: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර විභාග වන නඩු සංඛ්‍යාව ඇතුලත් කරන්න'
              },
              regexp: {
                regexp: /^[0-9]*$/,
                message: '<i class="fa fa-times-circle">&nbsp;</i>විභාග වන නඩු සංඛ්‍යාව ඉලක්කම් පමණක් විය යුතුයිි'
              }
            }
          },
          inmate_cat: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සිරකරුවන් වර්ගීකරණය ඇතුලත් කරන්න'
              }
            }
          },
          prison_frequency: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සිර ගත වූ වාර ප්‍රමාණය ඇතුලත් කරන්න'
              }
            }
          },
          offence: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර වරදක් තෝරන්න'
              }
            }
          },
          appeal: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ඇපිල් සිරකරුවෙක්ද යනවග ඇතුලත් කරන්න'
              }
            }
          },
          special: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර විශේෂ සිරකරුවෙක්ද යනවග ඇතුලත් කරන්න'
              }
            }
          },
          detention_order: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර රැඳවුම් නියෝග මත රඳවා ඇත්ද යනවග තෝරන්න'
              }
            }
          },
          literacy: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර සාක්ෂරතා හැකියාව ඇතුලත් කරන්න'
              }
            }
          },
          country: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ජාතිකත්වය ඇතුලත් කරන්න'
              }
            }
          },
          nationality: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ජාතිය ඇතුලත් කරන්න'
              }
            }
          },
          religion: {
            message: 'ඔබ යෙදූ අගය වලංගු නොවේ',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර ආගම් ඇතුලත් කරන්න'
              }
            }
          },
        }
    });

    $(document).ready(function () {
      $('#l_inm').addClass('active');
      $('#l_inm1').addClass('active');
    });
    </script>

  </body>
</html>
