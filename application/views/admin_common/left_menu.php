

<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>

      <li id='l_dash'><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard text-aqua"></i> <span>Dashboard</span></a></li>

      <li id='l_rep' class="treeview">
        <a href="#">
          <i class="fa fa-bar-chart text-aqua"></i> <span>Reports</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class=" treeview-menu">
          <li id='l_rep1'><a href="<?php echo base_url();?>reports/tabular_report"><i class="fa fa-circle-o text-orange"></i> Tabular Report</a></li>
        </ul>
      </li>

      <li id='l_summary'><a href="<?php echo base_url();?>admin/progress_summary"><i class="fa fa-hourglass-half text-aqua"></i> <span>Progress Summary</span></a></li>


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
