<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Developed by </b>&nbsp;<a> IT Devision, Department of Prisons </a>
  </div>
  <strong>Copyright &copy; <?php echo date('Y')?> <a href="#">Department of Prisons</a>. &nbsp;</strong> All rights
  reserved.
</footer>
