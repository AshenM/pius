<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>Empty Page</title>
    <?php $this->load->view('common/css.php'); ?>
  </head>

  <body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('common/header.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header body_text"><!-- content-header -->
          <h1>Dashboard<small>Control panel</small></h1>
        </section><!-- /.content-header -->

        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->

          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?>
  </body>
</html>
