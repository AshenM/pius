<!-- Modal -->
<div class="modal fade" id="include_inmate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span><i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;</span>රැඳවියා ඇතුලත් කර ගැනීම</h4>
      </div>
      <form class="" id="include_inmates" action="<?php echo base_url(); ?>inmates/include_inmate" method="POST" role="form" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="container-fluid">
            <div class="col-md-12">
              <label class="control-label col-md-5">සිර අංකය :</label>
              <label class="ajax_text control-label col-md-7"><span id="inmate_number">-</span></label>
              <input type="hidden" class="form-control" name="inmate_id" id="inmate_id" required>
              <input type="hidden" class="form-control" name="inmate_number" id="hide_inmate_number" required>
            </div>
            <div class="col-md-12">
              <label class="control-label col-md-5">නම :</label>
              <label class="ajax_text control-label col-md-7"><span id="inmate_name">-</span></label>
            </div>
            <div class="col-md-12">
              <label class="control-label col-md-5">බන්ධනාගාර ආයතනය :</label>
              <label class="ajax_text control-label col-md-7"><span id="institute">-</span></label>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn submit_btn btn-primary">ඇතුලත් කර ගන්න </button>
        </div>
      </form>
    </div>
  </div>
</div>
