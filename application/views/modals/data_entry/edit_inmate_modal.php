<!-- Modal -->
<div class="modal fade" id="edit_inmate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form class="form-horizontal" id="update_inmates" action="<?php echo base_url(); ?>inmates/update_inmates" method="POST" role="form" name="register" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"><span><i class="fa fa-user-plus"></i>&nbsp;&nbsp;</span>රැඳවියාගේ තොරතුරු යාවත්කාලීන කිරීම</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 bg-info">
                <div class="col-md-12 form-group">
                  <div class="col-md-3">
                    <h4><label class="" >සිර කරු මාරුකිරීම :</label></h4>
                  </div>
                  <div class='col-md-6'>
                    <input type="checkbox" class="" name="exchange" id="exchange" value='1'>
                  </div>
                </div>
              </div>
            </div>
            <div class="row"><br/>
              <div class="col-md-12 form-group exchange_show">
                <div class="col-md-4">
                  <label class="control-label" >මාරු කලයුතු බන්ධනාගාර ආයතනය *:</label>
                </div>
                <div class='col-md-8'>

                  <input type="hidden" class="form-control" name="inmate_id" id="inmate_id" value='' required>
                  <select class="form-control " name="institute" id="institute" required >
                    <option value=''></option>
                    <?php foreach($institutes as $row): ?>
                      <option value="<?php echo $row->institute_id; ?>" ><?php echo $row->institute_name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>

              </div>
              <div class="col-md-12 form-group exchange_show">
                <div class="col-md-4">
                  <label class="control-label" >මාරු කරන දිනය *:</label>
                </div>
                <div class='col-md-8'>
                  <input type="text" class="form-control date" name="exchange_date" id="exchange_date" data-provide="datepicker" placeholder="Year/Month/Date" required>
                </div>

              </div>
              <div class="col-md-6">


                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >සිර අංකය *:</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control" name="inmate_number" id="inmate_number" required>
                    <!-- <input type="" class="form-control" name="inmate_id" id="inmate_id" value='' required> -->
                  </div>
                </div>
                <!-- <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >සැක :</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="checkbox" class="" name="suspect" id="suspect" value='1'>
                  </div>
                </div> -->
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >නම *:</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control" name="inmate_name" id="inmate_name" required>
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >උපන් දිනය :</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control date" name="birthday" id="birthday" data-provide="datepicker" placeholder="Year/Month/Date">
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >ස්ත්‍රී පුරුෂ භාවය *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="පුරුෂ" name="gender" id="gender_male"> <lable>පුරුෂ &nbsp;&nbsp;</lable>
                      <input type="radio" value="ස්ත්‍රී" name="gender" id="gender_female"> <lable>ස්ත්‍රී</lable>
                    </span>
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >බන්ධනාගාර ගත වූ දිනය *:</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control date" name="prison_date" id="prison_date" data-provide="datepicker" placeholder="Year/Month/Date" required>
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් සහිතව):</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control date" name="release_date_rem" id="release_date_rem" data-provide="datepicker" placeholder="Year/Month/Date">
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් රහිතව):</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control date" name="release_date" id="release_date" data-provide="datepicker" placeholder="Year/Month/Date" >
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >උසාවිය *:</label>
                  </div>
                  <div class='col-md-8'>
                    <!-- <input type="text" class="form-control" name="court" id="court" required> -->
                    <select class="form-control select2" multiple name="court[]" id="court" required>
                      <option value=''></option>
                      <?php foreach($courts as $row): ?>
                        <option value="<?php echo $row->court_id; ?>" ><?php echo $row->court_name; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >විභාග වන නඩු සංඛ්‍යාව *:</label>
                  </div>
                  <div class='col-md-8'>
                    <input type="text" class="form-control" name="no_of_cases" id="no_of_cases" required>
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >සිරකරුවන් වර්ගීකරණය *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="සාමාන්‍ය" name="inmate_cat" id="inmate_cat_n"> <lable>සාමාන්‍ය &nbsp;&nbsp;</lable>
                      <input type="radio" value="ජීවිතාන්ත" name="inmate_cat" id="inmate_cat_l"> <lable>ජීවිතාන්ත &nbsp;&nbsp;</lable>
                      <input type="radio" value="ම.ද.නී" name="inmate_cat" id="inmate_cat_d"> <lable>ම.ද.නී</lable>
                    </span>
                  </div>
                </div>
                <div class="col-md-12 form-group exchange_hide">
                  <div class="col-md-4">
                    <label class="control-label" >සිර ගත වූ වාර ප්‍රමාණය *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="FO" name="prison_frequency" id="prison_frequency_fo"> <lable>FO &nbsp;&nbsp;</lable>
                      <input type="radio" value="RC" name="prison_frequency" id="prison_frequency_rc"> <lable>RC &nbsp;&nbsp;</lable>
                      <input type="radio" value="RRC" name="prison_frequency" id="prison_frequency_rrc"> <lable>RRC</lable>
                    </span>
                  </div>
                </div>

              </div>

              <!-- right side -->
              <div class="col-md-6 exchange_hide">


                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >වරද *:</label>
                  </div>
                  <div class='col-md-8'>
                    <select class="form-control select2" multiple='multiple' name="offence[]" id="offence" required>
                      <option value=''></option>
                      <?php foreach($offences as $row): ?>
                        <option value="<?php echo $row->offence_id; ?>" ><?php echo $row->offence; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >ඇපිල් සිරකරුවෙක්ද? *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="නැත" name="appeal" id="appeal_n"> <lable>නැත &nbsp;&nbsp;</lable>
                      <input type="radio" value="ඇපිල් කර ඇත" name="appeal" id="appeal_y"> <lable> ඇපිල් කර ඇත </lable>
                    </span>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >විශේෂ සිරකරුවෙක්ද? *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="සාමාන්‍ය" name="special" id="special_n"> <lable>සාමාන්‍ය &nbsp;&nbsp;</lable>
                      <input type="radio" value="විශේෂ" name="special" id="special_s"> <lable>විශේෂ </lable>
                    </span>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >රැඳවුම් නියෝග මත රඳවා ඇත්ද? *:</label>
                  </div>
                  <div class='col-md-8'>
                    <select class="form-control" name="detention_order" id="detention_order" required>
                      <option value=''></option>
                      <option value='නැත'>නැත</option>
                      <option value='JVP'>JVP</option>
                      <option value='LTTE'>LTTE</option>
                      <option value='NTJ'>NTJ</option>
                      <option value='වෙනත්'>වෙනත්</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >සාක්ෂරතා හැකියාව *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="ඇත" name="literacy" id="literacy_y"> <lable>ඇත &nbsp;&nbsp;</lable>
                      <input type="radio" value="නැත" name="literacy" id="literacy_n"> <lable>නැත</lable>
                    </span>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >ජාතිකත්වය *:</label>
                  </div>
                  <div class='col-md-8' >
                    <span>
                      <input type="radio" value="ශ්‍රී ලාංකික" name="country" id="country_sl"> <lable>ශ්‍රී ලාංකික &nbsp;&nbsp;</lable>
                      <input type="radio" value="වෙනත්" name="country" id="country_ot"> <lable>වෙනත්</lable>
                    </span>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >ජාතිය *:</label>
                  </div>
                  <div class='col-md-8'>
                    <select class="form-control" name="nationality" id="nationality" required>
                      <option value=''></option>
                      <option value='සිංහල'>සිංහල</option>
                      <option value='දෙමළ'>දෙමළ</option>
                      <option value='මුස්ලිම්'>මුස්ලිම්</option>
                      <option value='බර්ගර්'>බර්ගර්</option>
                      <option value='මැලේ'>මැලේ</option>
                      <option value='වෙනත්'>වෙනත්</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 form-group">
                  <div class="col-md-4">
                    <label class="control-label" >ආගම *:</label>
                  </div>
                  <div class='col-md-8'>
                    <select class="form-control" name="religion" id="religion" required>
                      <option value=''></option>
                      <option value='බෞද්ධ'>බෞද්ධ</option>
                      <option value='කතෝලික'>කතෝලික</option>
                      <option value='හින්දු'>හින්දු</option>
                      <option value='ඉස්ලාම්'>ඉස්ලාම්</option>
                      <option value='වෙනත්'>වෙනත්</option>
                    </select>
                  </div>
                </div>

              </div>

            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn submit_btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
