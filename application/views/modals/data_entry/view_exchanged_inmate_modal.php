<!-- Modal -->
<div class="modal fade" id="view_exchanged_inmate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span><i class="fa fa-user"></i>&nbsp;&nbsp;</span> නිදහස් කල රැඳවියාගේ තොරතුරු </h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="col-md-12">
            <label class="control-label col-md-5">සිර අංකය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_inmate_number">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">නම :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_inmate_name">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">බන්ධනාගාර ආයතනය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_institute">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">සැක :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_suspect">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">උපන් දිනය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_birthday">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">ස්ත්‍රී පුරුෂ භාවය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_gender">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">බන්ධනාගාර ගත වන විට වයස  :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_prisoned_age">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">බන්ධනාගාර ගත වූ දිනය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_prison_date">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">නිදහස ලැබීමට නියමිත දිනය (ලිහිල් කිරීම් සහිතව):</label>
            <label class="ajax_text control-label col-md-7"><span id="view_release_date">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">උසාවිය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_court">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">විභාග වන නඩු සංඛ්‍යාව :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_no_of_cases">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">සිරකරුවන් වර්ගීකරණය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_inmate_category">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">සිර ගත වූ වාර ප්‍රමාණය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_prison_frequency">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">වරද :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_offence">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">ඇපිල් සිරකරුවෙක්ද? :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_appeal">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">විශේෂ සිරකරුවෙක්ද? :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_special">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">රැඳවුම් නියෝග මත රඳවා ඇත්ද? :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_detention_order">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">සාක්ෂරතා හැකියාව :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_literacy">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">ජාතිකත්වය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_country">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">ජාතිය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_nationality">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">ආගම :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_religion">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">නිදහස් කල දිනය :</label>
            <label class="ajax_text control-label col-md-7"><span id="view_exchanged_date">-</span></label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
