<!-- Modal -->
<div class="modal fade" id="delete_inmate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" id="remove_inmates" action="<?php echo base_url(); ?>inmates/delete_inmate" method="POST" role="form"  enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"><span><i class="fa fa-user-times"></i>&nbsp;&nbsp;</span>රැඳවියා නිදහස් කිරීම / මරණයට පත්වීම්</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row"><br/>
              <input type="hidden" class="form-control" name="del_inmate_id" id="del_inmate_id" required>
              <div class="col-md-12 form-group">
                <div class="col-md-4">
                  <label class="control-label" >නිදහස් වන ආකාරය *:</label>
                </div>
                <div class='col-md-8'>
                  <!-- <input type="text" class="form-control" name="court" id="court" required> -->
                  <select class="form-control" id="release" name="release" required>
                    <option value=''></option>
                    <?php foreach($release as $row): ?>
                      <option value="<?php echo $row->release_type_id; ?>" ><?php echo $row->release_type; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-12 form-group">
                <div class="col-md-4">
                  <label class="control-label" >නිදහස්වූ දිනය / මරණයට පත්වූ දිනය *:</label>
                </div>
                <div class='col-md-8'>
                  <input type="text" class="form-control date" name="released_date" id="released_date" data-provide="datepicker" placeholder="Year/Month/Date" required>
                </div>
              </div>
              <div class="col-md-12 form-group">
                <div class="col-md-4">
                  <label class="control-label" >නැවත පැමිණෙන දිනය :</label>
                </div>
                <div class='col-md-8'>
                  <input type="text" class="form-control date" name="return_date" id="return_date" data-provide="datepicker" placeholder="Year/Month/Date" >
                </div>
              </div>
              <div class="col-md-12 form-group">
                <div class="col-md-4">
                  <label class="control-label" >විස්තරය :</label>
                </div>
                <div class='col-md-8'>
                  <textarea class="form-control" name="remark" id="remark" rows="3"></textarea>
                </div>
              </div>

            </div>
              <p style='color:green' >නැවත පැමිණෙන දිනයක් හෝ විස්තරයක් නොමැතිනම් එය හිස්ව තබන්න.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn submit_btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
