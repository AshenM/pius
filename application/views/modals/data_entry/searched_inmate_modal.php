<!-- Modal -->
<div class="modal fade" id="searched_inmate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span><i class="fa fa-user"></i>&nbsp;&nbsp;</span>රැඳවියාගේ තොරතුරු </h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="col-md-12">
            <label class="control-label col-md-5">සිර අංකය :</label>
            <label class="ajax_text control-label col-md-7"><span id="search_inmate_number">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">නම :</label>
            <label class="ajax_text control-label col-md-7"><span id="search_inmate_name">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">බන්ධනාගාර ගත වූ දිනය :</label>
            <label class="ajax_text control-label col-md-7"><span id="search_prison_date">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">අයත් බන්ධනාගාර ආයතනය :</label>
            <label class="ajax_text control-label col-md-7"><span id="search_institute">-</span></label>
          </div>
          <div class="col-md-12">
            <label class="control-label col-md-5">නිදහස් වූ අයෙක්ද බව :</label>
            <label class="ajax_text control-label col-md-7"><span id="search_release">-</span></label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
