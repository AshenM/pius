<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Gate'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>බන්ධනාගාරගත රැඳවියන් සෙවීම</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- data table -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('common/header.php'); ?>
      <?php $this->load->view('common/left_menu.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header body_text"><!-- content-header -->
          <h1 style='text-align:center'>බන්ධනාගාරගත රැඳවියන් සෙවීම</h1>
        </section><!-- /.content-header -->

        <section class="content"><!-- content-body-->
          <!-- data search area -->
            <div class='col-md-12 col-xs-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-search">&nbsp; </i>රැඳවියන්ගේ තොරතුරු සොයන්න</h3>
                </div>
                <div class="box-body ">
                  <form class="form-horizontal" id="search_inmates" action="<?php echo base_url(); ?>gate/search_inmates" method="POST" role="form" enctype="multipart/form-data">

                    <div class="col-md-12">
                      <div class="col-md-5">
                        <div class="col-md-12 form-group">
                          <label>සෙවුම් විකල්පය *:</label>
                          <select class="form-control" name="search_option" id="search_option" required>
                            <option value=''></option>
                            <option value='dup_inmate_number'>සිර අංකය</option>
                            <option value='inmate_name'>නම</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="col-md-12 form-group">
                          <label>සෙවුම *:</label>
                          <input type="text" class="form-control" name="search_data" id="search_data" required min-lingth='4'>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="col-md-12 form-group">
                          <label>&nbsp;</label>
                          <button type="button" id='search_btn' class="btn btn-primary"><i class="fa fa-search">&nbsp; </i>Search </button>
                        </div>

                      </div>
                    </div>

                  </form>

                </div>

              </div>
            </div>

          <div class="row">
            <div class='col-md-12'>
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-user-o">&nbsp; </i>රැඳවියන්ගේ තොරතුරු </h3>
                </div>
                  
                <div class="box-body">
                    <h3 id="search_release" style="text-align:center; color:red"></h3>
                  <div id='data_result'></div>
                </div>

              </div>
            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?><!-- DataTables -->
    <script src="<?php echo base_url(); ?>theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <script>
      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
		
		
      });

      //data table load
      //$(document).ready(function(){
      //  $('#tbl_inmates').DataTable({
      //    "responsive": true,
      //    "dom": 'Bfrtip',
      //    "autoWidth": false
      //  });
      //});
	  
	  function table(){
		$('#tbl_inmates').DataTable({
          "responsive": true,
          "dom": 'Bflrtip',
          "autoWidth": false
        });
		}

      // prevent form submit by keyboard enter

      $('#search_inmates').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          return false;
        }
      });

      //validate estimate inputs
      function validate_function(){
        var search_option = $('#search_option').val().trim();
        var search_data = $('#search_data').val().trim();
        //var regex=new RegExp('^[0-9]$');;
        //console.log(unit);
        if( search_option == '' || search_option ==null){
          //toastr.error('Building Material Description is required');
          return false;
        }
        if( search_data == '' || search_data ==null){
          //toastr.error('Unit is required');
          return false;
        }else{
          return true;
        }
      }

      //ajax for data search
      $(document).on('click', '#search_btn', function (e) {
        e.preventDefault();
        $("#data_result").empty();
        var validation = validate_function();
        //console.log(validation);
        if(validation){
          var search_option = $('#search_option').val().trim();
          var search_data = $('#search_data').val().trim();
          //removing whitespace from search_string
          var search_option= search_option.replace(/^\s+|\s+$/gm,'');
          //console.log(search_option);
          if(search_option !='' && search_data != ''){
          //  console.log(search_data);
            $.ajax({
              url: $('#search_inmates').attr('action'),
              method: "POST",
              dataType: 'JSON',
              data: {search_option: search_option, search_data:search_data },
              success: function (data) {
               //console.log(data);
                if(data){
                  $('#data_result').html(data);
				  table();
                }else {
                  $('#search_release').text('ඔබ සෙවූ තොරතුරු වලට අදාලව රැඳවියෙකු දැනට නොමැත');
                }
              }
            });

          }
        }

        //console.log(view_key);
      });

    </script>

  </body>
</html>
