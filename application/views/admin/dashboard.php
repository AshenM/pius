<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Admin'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('admin_common/head_meta.php'); ?>
    <title>රැඳවියන්ගේ තොරතුරු</title>
    <?php $this->load->view('admin_common/admin_css.php'); ?>
    
    <style>
      .institute_link {
        color:#fff;
        text-decoration: none; /* no underline */
      }
      .institute_link:hover {
        color: #191970;
      }
    </style>
  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('admin_common/admin_header.php'); ?>
      <?php $this->load->view('admin_common/left_menu.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header body_text"><!-- content-header -->
          <!-- <h1 >රැඳවියන්ගේ තොරතුරු</h1> -->
        </section><!-- /.content-header -->

        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->
            <div class='col-md-4 col-sm-6 col-xs-12'>

              <!-- total inmates box -->
              <div class="col-md-12">
                <div class="info-box  bg-aqua">
                  <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>
                  <div class="info-box-content ">
                    <span class="info-box-text">Total Convicted Inmates</span>
                    <span class="info-box-number"><?php echo $total_inmates ?></span>
                    <span class="">In Transit - <?php echo $total_intransit ?></span>
                  </div>
                </div>
              </div>
              <!-- total inmates box -->
              <div class="col-md-12">
                <div class="col-md-12 bg-green">
                  <br/><span class="info-box-text">Total Inmates in Each Prsion</span>

                  <?php foreach($total_institutes as $row): ?>
                      <hr/> <a href='<?php echo base_url(); ?>admin/single_institute/<?= $row->institute ?>' class='institute_link' target='_blank'><p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p></a>

                  <?php endforeach; ?>
                </div>
              </div>

            </div>

            <div class='col-md-8 col-sm-6 col-xs-12'>
              <!-- row 1 -->
              <div class="row">
                <!-- gender wise pie chart -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Inmates by Gender</p>
                      <canvas id="bygender_chart" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
                <!--  -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Classification of Prisoners</p>
                      <canvas id="inmate_category" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
              </div>

              <!-- row 2 -->
              <div class="row">
                <!-- gender wise pie chart -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Frequency of incarceration</p>
                      <canvas id="prison_frequency" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
                <!--  -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Appeal and Non-appeal Inmates</p>
                      <canvas id="appeal_inmates" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
              </div>

              <!-- row 3 -->
              <div class="row">
                <!-- gender wise pie chart -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Special and Normal Inmates</p>
                      <canvas id="special_inmate" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
                <!--  -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Literacy of Inmates</p>
                      <canvas id="inmate_literacy" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
              </div>

              <!-- row 4 -->
              <div class="row">
                <!-- gender wise pie chart -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Restrained on detention orders</p>
                      <canvas id="detention_order" style="height:'auto'"></canvas>
                    </div>
                  </div>
                </div>
                <!--  -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Inmates by Nationality</p>
                      <canvas id="nationality" style="height: 'auto'"></canvas>
                    </div>
                  </div>
                </div>
              </div>

              <!-- row 5 -->
              <div class="row">
                <!-- gender wise pie chart -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Inmates by Nation</p>
                      <canvas id="nation"  height='200'></canvas>
                    </div>
                  </div>
                </div>
                <!--  -->
                <div class="col-md-6">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Inmates by Religion</p>
                      <canvas id="religion" height='200'></canvas>
                    </div>
                  </div>
                </div>
              </div>

              <!-- row 6 -->
              <div class="row">
                <!-- gender wise pie chart -->
                <div class="col-md-12">
                  <div class="box">
                    <div class="box-body">
                      <p class="info-box-text">Inmates by Age</p>
                      <canvas id="inmates_byage" style="height: 'auto'"></canvas>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->
      <!-- view,edit modals -->


      <?php $this->view('admin_common/admin_footer.php'); ?>
    </div>
    <?php $this->view('admin_common/admin_js.php');?>
    <!-- ChartJS -->
    <script src="<?php echo base_url();?>theme/thirdparty/chartjs/Chart.js"></script>
    <script>
      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
      });


      //doughnut pie for gender wise inmates
      $(document).ready(function(){
        //pie chart for gender wise inmates
        var ctx = $('#bygender_chart');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: [<?php foreach($gender_chart as $row){echo "'".$row->gender."',";}?>],
            datasets: [{
              data: [<?php foreach($gender_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#9818d6','#ff5151']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for inmate category
        var ctx = $('#inmate_category');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: [<?php foreach($inmatecat_chart as $row){echo "'".$row->inmate_category."',";}?>],
            datasets: [{
              data: [<?php foreach($inmatecat_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#217a49','#f7a979','#dd8205']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for prison frequency
        var ctx = $('#prison_frequency');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: [<?php foreach($prison_frequency_chart as $row){echo "'".$row->prison_frequency."',";}?>],
            datasets: [{
              data: [<?php foreach($prison_frequency_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#527318','#eb8242','#f7be16']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for appeal_prisoners
        var ctx = $('#appeal_inmates');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: ['Appeal','Non-appeal'],
            datasets: [{
              data: [<?php foreach($appeal_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#9aceff','#4a69bb']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for appeal_prisoners
        var ctx = $('#special_inmate');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: ['Special', 'Normal'],
            datasets: [{
              data: [<?php foreach($special_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#ec9b99','#f75e14']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for appeal_prisoners
        var ctx = $('#inmate_literacy');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: ['Literate', 'Illiterate'],
            datasets: [{
              data: [<?php foreach($literacy_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#9852f9','#c299fc']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for detention_order
        var ctx = $('#detention_order');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: [<?php foreach($detention_chart as $row){echo "'".$row->detention_order."',";}?>],
            datasets: [{
              data: [<?php foreach($detention_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#552244','#596157','#5b8c5a','#cfd186','#ffbd69']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //pie chart for Nationality
        var ctx = $('#nationality');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: ['Sri Lankan', 'Foreign'],
            datasets: [{
              data: [<?php foreach($country_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#feb72b','#527318']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'left'
            }
    			}
        });

        //bar chart for nation
        var ctx = $('#nation');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: [<?php foreach($nation_chart as $row){echo "'".$row->nationality."',";}?>],
            datasets: [{
              data: [<?php foreach($nation_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#9818d6','#ff5151','#ffa41b','#05dfd7','#ec7373','#ffe196']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'top'
            }
    			}
        });

        //bar chart for religion
        var ctx = $('#religion');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: [<?php foreach($religion_chart as $row){echo "'".$row->religion."',";}?>],
            datasets: [{
              data: [<?php foreach($religion_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#ffcc00','#ff6666','#cc0066','#66cccc','#263f44']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              position: 'top'
            }
    			}
        });

        //bar chart for inmates bya age
        var ctx = $('#inmates_byage');
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: [<?php foreach($age_chart as $row){echo "'".$row->range."',";}?>],
            datasets: [{
              data: [<?php foreach($age_chart as $row){echo $row->amount.",";}?>],
              borderWidth: 1,
              backgroundColor: ['#ffcc00','#ff6666','#cc0066','#66cccc','#263f44','#33663b','#9818d6']
            }]
          },
    			options: {
    				responsive: true,
            legend: {
              display: false,
              position: 'top'
            }
    			}
        });

      });

      $(document).ready(function () {
        $('#l_dash').addClass('active');
      });

    </script>

  </body>
</html>
