<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Admin'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('admin_common/head_meta.php'); ?>
    <title>Progress Summary</title>
    <?php $this->load->view('admin_common/admin_css.php'); ?>

  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php $this->load->view('admin_common/admin_header.php'); ?>
      <?php $this->load->view('admin_common/left_menu.php'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header" style="color:#bbb; font-size:20px">
          <h1>Progress Summary</h1>
        </section>

        <section class="content"><!-- content-body-->
          <div class="row">
            <div class="col-md-4">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">ආයතන මගින් සිරමාරු ඇතුලත් කර නොගැනීම් (In-transit)</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?php foreach($intransit as $row): ?>
                      <p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p><hr/>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">අද දින නව ඇතුලත් කිරීම් ප්‍රමාණය (New Entry)</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?php foreach($today_enter as $row): ?>
                      <p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p><hr/>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">අද දින ආයතන වෙත මාරු කල ප්‍රමාණය (Transfered)</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?php foreach($today_transfered as $row): ?>
                      <p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p><hr/>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">අද දින ආයතන වෙත මාරුවී පැමිණිම් (Recieved)</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?php foreach($today_recieved as $row): ?>
                      <p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p><hr/>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">අද දින නිදහස් කල ප්‍රමාණය (Released)</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?php foreach($today_release as $row): ?>
                      <p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p><hr/>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">අද දින මරණයට පත් ප්‍රමාණය (Dead)</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <?php foreach($today_death as $row): ?>
                      <p><?= $row->institute_name ?> - <span > <?= $row->amount ?></span></p><hr/>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>

          </div>

        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->
      <!-- view,edit modals -->


      <?php $this->view('admin_common/admin_footer.php'); ?>
    </div>
    <?php $this->view('admin_common/admin_js.php');?>
    <script>

      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            timer: 2000,
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
      });




      $(document).ready(function () {
        $('#l_summary').addClass('active');
      });

    </script>

  </body>
</html>
