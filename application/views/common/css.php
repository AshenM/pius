<!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>theme/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>theme/dist/css/skins/_all-skins.min.css">

  <!-- Admin lte css overwrite -->
  <link rel="stylesheet" href="<?php echo base_url();?>theme/overwrite/child_theme/overwrite_css.css">
  <!-- sweetalert2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>theme/thirdparty/sweetalert2/dist/sweetalert2.min.css">
