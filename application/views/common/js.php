<!-- jQuery 3 -->
<script src="<?php echo base_url();?>theme/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>theme/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>theme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url();?>theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>theme/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>theme/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url();?>theme/overwrite/child_theme/overwrite_js.js"></script>
<!-- sweetalert2 -->
<script src="<?php echo base_url();?>theme/thirdparty/sweetalert2/dist/sweetalert2.all.min.js"></script>
