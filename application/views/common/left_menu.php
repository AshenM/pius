<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    $ses_user_type=$session_data['ses_user_type'];
  }
?>

<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <?php if($ses_user_type == 'Data Entry'): ?>
      <!-- /.search form -->
      <form action="<?php echo base_url(); ?>inmates/search_inmate" method="post" id="search_form" class="sidebar-form">
        <div class="input-group">
          <input type="text" id="search_string" name="search_string" class="form-control" autocomplete="off" placeholder="සිර අංකය මගින් සොයන්න..." required>
          <span class="input-group-btn">
            <button type="button" name="search_btn" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </form>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ප්‍රධාන පාලකය</li>

        <li id='l_dash'><a href="<?php echo base_url();?>inmates/dashboard"><i class="fa fa-dashboard"></i> <span>මුල් පුවරුව</span></a></li>

        <li id='l_inm' class="treeview">
          <a href="#">
            <i class="fa fa-group text-aqua"></i> <span>සිර කරුවන්</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class=" treeview-menu">
            <li id='l_inm1'><a href="<?php echo base_url();?>inmates/add_inmates"><i class="fa fa-circle-o text-orange"></i> නව රැඳවියන් ඇතුලත් කිරීම</a></li>
            <li id='l_inm2'><a href="<?php echo base_url();?>inmates/view_inmates"><i class="fa fa-circle-o text-orange"></i> දැනට සිටින රැඳවියන්</a></li>
            <li id='l_inm3'><a href="<?php echo base_url();?>inmates/recieved_inmates"><i class="fa fa-circle-o text-orange"></i> මාරු වී පැමිණි රැඳවියන්</a></li>
            <li id='l_inm4'><a href="<?php echo base_url() ?>inmates/exchanged_inmates"><i class="fa fa-circle-o text-orange"></i> මාරු කල රැඳවියන්</a></li>
            <li id='l_inm5'><a href="<?php echo base_url() ?>inmates/deleted_inmates"><i class="fa fa-circle-o text-orange"></i> නිදහස් කල රැඳවියන්</a></li>
          </ul>
        </li>

        <li id='l_rep' class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart text-aqua"></i> <span>වාර්තා</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class=" treeview-menu">
            <li id='l_rep1'><a href="<?php echo base_url();?>reports/tabular_report"><i class="fa fa-circle-o text-orange"></i> අනුක්‍රමණික වාර්තා</a></li>
            <li id='l_rep2'><a href="<?php echo base_url();?>reports/transfer_report"><i class="fa fa-circle-o text-orange"></i> සිර මාරු වාර්තා</a></li>
          </ul>
        </li>

      </ul>
    }
  <?php endif; ?>

  </section>
  <!-- /.sidebar -->
</aside>

<?php $this->view('modals/data_entry/searched_inmate_modal.php'); ?>
