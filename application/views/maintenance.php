<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Site Under Maintenance</title>
    <style>
      .maintenance-page{
        height: auto;
        background-size: cover;
        background-position: center;
         background-repeat: no-repeat;
         background-attachment: fixed;
        background-image: url("http://124.43.129.95/pius/images/maintain.jpg");
      }
      h1{
        text-align: center;
        color: #fff;
        font-size: 80px;
      }
      p{
        text-align: center;
        color: #fff;
        font-size: 40px;
      }
    </style>
</head>
<body class=" maintenance-page">
    <h1 class="head text-center">සමාවන්න! </h1>
    <h1 class="head text-center">පද්ධතියේ නඩත්තු කටයුතු සඳහා තාවකාලිකව ක්‍රියාවිරහිත කර තිබේ.</h1>
    <p>පසුව උත්සහ කර බලන්න</p>
</body>
</html>
