<?php
  //destroy already created session data
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
      $this->session->unset_userdata("loggedin_user");
      $this->session->sess_destroy("loggedin_user");
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>රැඳවි තොරතුරු යාවත්කාලීන කිරීම 2019/20</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- bootstrap validator -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/css/bootstrapValidator.min.css">
    <!-- sweetalert -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/sweetalert2/dist/sweetalert2.min.css">
    <style>
      .login-page{
        height: auto;
        background-size: cover;
        background-position: center;
        background-image: url("<?php echo base_url(); ?>images/login-bg.jpg");
      }
      .login-logo{
        color:#fff;
      }
      .form-group.has-error .help-block {
        color: orange;
      }
      .form-group.has-error label {
        color: #ddd;
      }
    </style>
  </head>
  <body class="hold-transition login-page ">
    <div class="login-box">
    <div class="login-logo">
      <b>රැඳවි තොරතුරු යාවත්කාලීන කිරීම </b>2019/20
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">ඔබගේ පරිශීලක නාමය සහ මුර පදය ඇතුලත් කරන්න</p>

      <form id="login_form" action="<?php echo base_url(); ?>login/check_login" method="post" role="form" name="login_form" enctype="multipart/form-data">
        <div class="form-group has-feedback">
          <label class="control-label" ><h4>පරිශීලක නාමය</h4></label>
          <input type="text" name='username' id='username' class="form-control" placeholder="Username">
        </div>
        <div class="form-group has-feedback">
          <label class="control-label" ><h4>මුර පදය</h4></label>
          <input type="password" name='password' id='password' class="form-control" placeholder="Password">
        </div>
        <div class="row">
          <div class="col-xs-4 pull-right">
            <button type="submit" class="btn btn-primary btn-block  btn-flat">Log in </button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.login-box-body -->
  </div>
    <!-- /.login-box -->

    <?php $this->view('common/js.php');?>
    <!-- bootstrap validator -->
    <script src="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/js/bootstrapValidator.js"></script>
    <!-- sweetaler -->
    <script src="<?php echo base_url(); ?>theme/thirdparty/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/sweetalert2/dist/sweetalert2.all.min.js"></script>

    <script>
      //sweet alert fire on response
      $(document).ready(function(){
        <?php if ($this->session->flashdata('error')): ?>
          swal.fire({
            type: 'error',
            title: 'සමාවන්න!',
            text: '<?php echo $this->session->flashdata('error'); ?>'
          });
        <?php  elseif($this->session->flashdata('success')): ?>
          swal.fire({
            type: 'success',
            title: 'සාර්ථකයිි !',
            text: '<?php echo $this->session->flashdata('success'); ?>'
          });
        <?php  endif; ?>
      });

      // bootstrapValidator
      $('#login_form').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
          username: {
            message: 'The Username is required',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර පරිශීලක නාමය ඇතුලත් කරන්න '
              }
            }
          },
          password: {
            message: 'The Password is required',
            validators: {
              notEmpty: {
                message: '<i class="fa fa-times-circle">&nbsp;</i>කරුණාකර මුර පදය ඇතුලත් කරන්න '
              }
            }
          }
        }
    });
    </script>
  </body>
</html>
