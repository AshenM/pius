<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Data Entry' && $ses_user_type != 'Admin'){
    show_404();
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>Report</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- data table -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/datatables.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Buttons-1.6.1/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/ColReorder-1.5.2/css/colReorder.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Responsive-2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Scroller-2.0.1/css/scroller.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/SearchPanes-1.0.1/css/searchPanes.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Select-1.3.1/css/select.bootstrap.min.css">

    <!-- date picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- bootstrap validator -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/css/bootstrapValidator.min.css">

    <style>
      .srh_input {
        background-color: #eee;
        padding: 7px 10px;
        display: inline-block;
        border: 1px solid #222;
        border-radius: 4px;
        box-sizing: border-box;
        color: #666;
      }
    </style>
  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php  ?>
      <?php
        if($ses_user_type=='Data Entry'){
          $this->load->view('common/header.php');
          $this->load->view('common/left_menu.php');
        }else if($ses_user_type =='Admin'){
          $this->load->view('admin_common/admin_header.php');
          $this->load->view('admin_common/left_menu.php');
        }
      ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->
            <div class='col-md-12 col-xs-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-table"> </i> රැඳවියන්ගේ තොරතුරු </h3>
                </div>
                <div class="box-body table-responsive">
                  <table id="tbl_inmates" class="table table-bordered table-striped ">
                    <thead>
                      <tr>
                        <th>සිර අංකය</th>
                        <th>නම</th>
                        <th>ස්ත්‍රී පුරුෂ භාවය</th>
                        <th>උපන් දිනය</th>
                        <th>වයස</th>
                        <th>වයස් පරතරය</th>
                        <th>සිරගතවන විට වයස</th>
                        <th>සිරගත වූ දිනය</th>
                        <th>නිදහස් වන දිනය (සමා රහිත)</th>
                        <th>නිදහස් වන දිනය (සමා සහිත)</th>
                        <th>බන්ධනාගාර ආයතනය</th>
                        <th>සිරකරුවන් වර්ගීකරණය</th>
                        <th>සිර ගත වූ වාර ප්‍රමාණය</th>
                        <th>ඇපිල් ද?</th>
                        <th>විශේෂ සිරකරුවෙක්ද?</th>
                        <th>විභාග වන නඩු සංඛ්‍යාව</th>
                        <th>රැඳවුම් නියෝග මත රඳවා ඇත්ද?</th>
                        <th>උසාවි</th>
                        <th>වැරදි</th>
                        <th>සාක්ෂරතා හැකියාව</th>
                        <th>ජාතිකත්වය</th>
                        <th>ජාතිය</th>
                        <th>ආගම</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($inmates as $row): ?>
                          <tr>
                            <td><?= $row->dup_inmate_number ?></td>
                            <td><?= $row->inmate_name ?></td>
                            <td><?= $row->gender ?></td>
                            <td><?php if($row->birthday == '0000-00-00'){echo '-';} else {echo $row->birthday;} ?></td>
                            <td><?= $row->age ?></td>
                            <td><?= $row->age_range ?></td>
                            <td><?= $row->prisoned_age ?></td>
                            <td><?= $row->prison_date ?></td>
                            <td><?php if($row->release_date == '0000-00-00'){echo '-';} else {echo $row->release_date;} ?></td>
                            <td><?php if($row->release_date_rem == '0000-00-00'){echo '-';} else {echo $row->release_date_rem;} ?></td>
                            <td><?= $row->institute_name ?></td>
                            <td><?= $row->inmate_category ?></td>
                            <td><?= $row->prison_frequency ?></td>
                            <td><?= $row->appeal ?></td>
                            <td><?= $row->special ?></td>
                            <td><?= $row->no_of_cases ?></td>
                            <td><?= $row->detention_order ?></td>
                            <td><?= $row->court_name ?></td>
                            <td><?= $row->offence ?></td>
                            <td><?= $row->literacy ?></td>
                            <td><?= $row->country ?></td>
                            <td><?= $row->nationality ?></td>
                            <td><?= $row->religion ?></td>
                          </tr>
                        <?php endforeach;  ?>
                    </tbody>
                    <tfoot style="background-color:#bbb">
                      <tr>
                        <th>සිර අංකය </th>
                        <th>නම</th>
                        <th>ස්ත්‍රී පුරුෂ භාවය</th>
                        <th>උපන් දිනය</th>
                        <th>වයස</th>
                        <th>වයස් පරතරය</th>
                        <th>සිරගතවන විට වයස</th>
                        <th>සිරගත වූ දිනය</th>
                        <th>නිදහස් වන දිනය (සමා රහිත)</th>
                        <th>නිදහස් වන දිනය (සමා සහිත)</th>
                        <th>බන්ධනාගාර ආයතනය</th>
                        <th>සිරකරුවන් වර්ගීකරණය</th>
                        <th>සිර ගත වූ වාර ප්‍රමාණය</th>
                        <th>ඇපිල් ද?</th>
                        <th>විශේෂ සිරකරුවෙක්ද?</th>
                        <th>විභාග වන නඩු සංඛ්‍යාව</th>
                        <th>රැඳවුම් නියෝග මත රඳවා ඇත්ද?</th>
                        <th>උසාවි</th>
                        <th>වැරදි</th>
                        <th>සාක්ෂරතා හැකියාව</th>
                        <th>ජාතිකත්වය</th>
                        <th>ජාතිය</th>
                        <th>ආගම</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>
            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->
      <!-- view,edit modals -->
      <?php $this->view('modals/data_entry/view_removed_inmate_modal.php'); ?>

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?><!-- DataTables -->
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/datatables.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Buttons-1.6.1/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/ColReorder-1.5.2/js/colReorder.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Responsive-2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Scroller-2.0.1/js/scroller.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/SearchPanes-1.0.1/js/searchPanes.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Select-1.3.1/js/select.bootstrap.min.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap validator -->
    <script src="<?php echo base_url();?>theme/thirdparty/bootstrapvalidator/dist/js/bootstrapValidator.js"></script>
    <script>
          //data table load
      $(document).ready(function(){

        $('#tbl_inmates tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( title+ '<br/><input class=" srh_input" type="text" placeholder="Search..." />' );
        });

        var tb =$('#tbl_inmates').DataTable({
                //responsive: true, //responsive
                colReorder: true, // columns ColReorder
                scrollX: true,
                scrollY: "450px",
                scrollCollapse: true,
                paging:false,
                select: true,
                buttons: [
                  'colvis',
                  {
                    extend: 'copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                  },
                  {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                  },
                  {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                  }
                ],
                searchPanes:{
                  cascadePanes: true
                },
                dom: 'BPflrtip',
                columnDefs:[
                  {
                      searchPanes:{
                          show: false,
                      },
                      targets: [3,7,8,9,16,17,18,19],
                  },
              ],
              });


        tb.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
                .search( this.value )
                .draw();
            }
          } );
        } );
      });




      $(document).ready(function () {
        $('#l_rep').addClass('active');
        $('#l_rep1').addClass('active');
      });
    </script>

  </body>
</html>
