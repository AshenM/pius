<?php
  if ($this->session->has_userdata('loggedin_user') == TRUE) {
    $session_data=$this->session->userdata('loggedin_user');
    //var_dump($session_data);die;
    $ses_user=$session_data['ses_user'];
    $ses_institute_name=$session_data['ses_institute_name'];
    $ses_institute_id=$session_data['ses_institute_id'];
    $ses_user_type=$session_data['ses_user_type'];
  }
  if($ses_user_type != 'Data Entry' && $ses_user_type != 'Admin'){
    show_404();
  }
  $from_institute=null;
  $to_institute=null;
  $from_date=null;
  $to_date=null;
  if($feedback_data != null){
    foreach($feedback_data as $row){
      $from_institute= $feedback_data['from_institute'];
      $to_institute= $feedback_data['to_institute'];
      $from_date= $feedback_data['from_date'];
      $to_date= $feedback_data['to_date'];
    }
  }
?>


<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('common/head_meta.php'); ?>
    <title>Report | Transfer Report</title>
    <?php $this->load->view('common/css.php'); ?>
    <!-- data table -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/datatables.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Buttons-1.6.1/css/buttons.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/ColReorder-1.5.2/css/colReorder.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Responsive-2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Scroller-2.0.1/css/scroller.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>theme/thirdparty/data_table/Select-1.3.1/css/select.bootstrap.min.css">

    <!-- date picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <style>
      .srh_input {
        background-color: #eee;
        padding: 7px 10px;
        display: inline-block;
        border: 1px solid #222;
        border-radius: 4px;
        box-sizing: border-box;
        color: #666;
      }
    </style>
  </head>

  <body class="hold-transition skin-blue fixed sidebar-collapse sidebar-mini">
    <div class="wrapper">
      <!-- Header. contains the logo and profile picture -->
      <?php  ?>
      <?php
        if($ses_user_type=='Data Entry'){
          $this->load->view('common/header.php');
          $this->load->view('common/left_menu.php');
        }else if($ses_user_type =='Admin'){
          $this->load->view('admin_common/admin_header.php');
          $this->load->view('admin_common/left_menu.php');
        }
      ?>


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content"><!-- content-body-->
          <div class="row"><!-- row (main row) -->
            <div class='col-md-12 col-xs-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-search"> </i> සිර මාරු තොරතුරු සොයන්න</h3>
                </div>
                <div class="box-body ">
                  <form class="form-horizontal" id="register" action="<?php echo base_url(); ?>reports/transfer_report" method="POST" role="form" name="transfer" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6">
                        <label>පෙර සිටි ආයතනය *:</label>
                        <select class="form-control" name="from_institute" id="from_institute" required>
                          <option value=''></option>
                          <?php foreach($institutes as $row): ?>
                            <option value="<?php echo $row->institute_id; ?>" ><?php echo $row->institute_name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="col-md-6">
                        <label>මාරු වූ ආයතනය *:</label>
                        <select class="form-control" name="to_institute" id="to_institute" required>
                          <option value=''></option>
                          <?php foreach($institutes as $row): ?>
                            <option value="<?php echo $row->institute_id; ?>" ><?php echo $row->institute_name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>සිට (දිනය) :</label>
                        <input type="text" class="form-control date" name="from_date" id="from_date" data-provide="datepicker" placeholder="Year/Month/Date" readonly>
                      </div>
                      <div class="col-md-6">
                        <label>තෙක් (දිනය) :</label>
                        <input type="text" class="form-control date" name="to_date" id="to_date" data-provide="datepicker" placeholder="Year/Month/Date" readonly>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-search"> </i> Search </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>  <!-- /.row (main row) -->

          <div class="row"><!-- row (main row) -->
            <div class='col-md-12 col-xs-12'>
              <div class="box ">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-table"> </i> ආසන්නතම සිර මාරු තොරතුරු </h3>
                </div>
                <div class="box-body ">
                  <div class="row table-responsive">
                    <table id="tbl_inmates" class="table table-bordered table-striped ">
                      <thead>
                        <tr>
                          <th>සිර අංකය</th>
                          <th>නම</th>
                          <th>ස්ත්‍රී පුරුෂ භාවය</th>
                          <th>උපන් දිනය</th>
                          <th>වයස</th>
                          <th>වයස් පරතරය</th>
                          <th>සිරගතවන විට වයස</th>
                          <th>සිරගත වූ දිනය</th>
                          <th>නිදහස් වන දිනය (සමා රහිත)</th>
                          <th>නිදහස් වන දිනය (සමා සහිත)</th>
                          <th>බන්ධනාගාර ආයතනය</th>
                          <th>සිරකරුවන් වර්ගීකරණය</th>
                          <th>සිර ගත වූ වාර ප්‍රමාණය</th>
                          <th>ඇපිල් ද?</th>
                          <th>විශේෂ සිරකරුවෙක්ද?</th>
                          <th>විභාග වන නඩු සංඛ්‍යාව</th>
                          <th>රැඳවුම් නියෝග මත රඳවා ඇත්ද?</th>
                          <th>උසාවි</th>
                          <th>වැරදි</th>
                          <th>සාක්ෂරතා හැකියාව</th>
                          <th>ජාතිකත්වය</th>
                          <th>ජාතිය</th>
                          <th>ආගම</th>
                          <th>සිරමාරු කල දිනය</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if($transfers!= null):?>
                          <?php foreach($transfers as $row): ?>
                              <tr>
                                <td><?= $row->dup_inmate_number ?></td>
                                <td><?= $row->inmate_name ?></td>
                                <td><?= $row->gender ?></td>
                                <td><?php if($row->birthday == '0000-00-00'){echo '-';} else {echo $row->birthday;} ?></td>
                                <td><?= $row->age ?></td>
                                <td><?= $row->age_range ?></td>
                                <td><?= $row->prisoned_age ?></td>
                                <td><?= $row->prison_date ?></td>
                                <td><?php if($row->release_date == '0000-00-00'){echo '-';} else {echo $row->release_date;} ?></td>
                                <td><?php if($row->release_date_rem == '0000-00-00'){echo '-';} else {echo $row->release_date_rem;} ?></td>
                                <td><?= $row->institute_name ?></td>
                                <td><?= $row->inmate_category ?></td>
                                <td><?= $row->prison_frequency ?></td>
                                <td><?= $row->appeal ?></td>
                                <td><?= $row->special ?></td>
                                <td><?= $row->no_of_cases ?></td>
                                <td><?= $row->detention_order ?></td>
                                <td><?= $row->court_name ?></td>
                                <td><?= $row->offence ?></td>
                                <td><?= $row->literacy ?></td>
                                <td><?= $row->country ?></td>
                                <td><?= $row->nationality ?></td>
                                <td><?= $row->religion ?></td>
                                <td><?= $row->exchange_date ?></td>
                              </tr>
                            <?php endforeach;  ?>
                        <?php endif;?>
                      </tbody>
                      <tfoot style="background-color:#bbb">
                        <tr>
                          <th>සිර අංකය </th>
                          <th>නම</th>
                          <th>ස්ත්‍රී පුරුෂ භාවය</th>
                          <th>උපන් දිනය</th>
                          <th>වයස</th>
                          <th>වයස් පරතරය</th>
                          <th>සිරගතවන විට වයස</th>
                          <th>සිරගත වූ දිනය</th>
                          <th>නිදහස් වන දිනය (සමා රහිත)</th>
                          <th>නිදහස් වන දිනය (සමා සහිත)</th>
                          <th>බන්ධනාගාර ආයතනය</th>
                          <th>සිරකරුවන් වර්ගීකරණය</th>
                          <th>සිර ගත වූ වාර ප්‍රමාණය</th>
                          <th>ඇපිල් ද?</th>
                          <th>විශේෂ සිරකරුවෙක්ද?</th>
                          <th>විභාග වන නඩු සංඛ්‍යාව</th>
                          <th>රැඳවුම් නියෝග මත රඳවා ඇත්ද?</th>
                          <th>උසාවි</th>
                          <th>වැරදි</th>
                          <th>සාක්ෂරතා හැකියාව</th>
                          <th>ජාතිකත්වය</th>
                          <th>ජාතිය</th>
                          <th>ආගම</th>
                          <th>සිරමාරු කල දිනය</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>  <!-- /.row (main row) -->
        </section><!-- /.content-body-->

      </div><!-- /.content-wrapper -->

      <?php $this->view('common/footer.php'); ?>
    </div>
    <?php $this->view('common/js.php');?><!-- DataTables -->
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/datatables.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Buttons-1.6.1/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/ColReorder-1.5.2/js/colReorder.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Responsive-2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Scroller-2.0.1/js/scroller.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/thirdparty/data_table/Select-1.3.1/js/select.bootstrap.min.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url();?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
      //data table load
      $(document).ready(function(){

        $('#tbl_inmates tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( title+ '<br/><input class=" srh_input" type="text" placeholder="Search..." />' );
        });

        var tb =$('#tbl_inmates').DataTable({
                //responsive: true, //responsive
                colReorder: true, // columns ColReorder
                scrollX: true,
                scrollY: "450px",
                scrollCollapse: true,
                paging:false,
                select: true,
                buttons: [
                  'colvis',
                  {
                    extend: 'copy',
                    exportOptions: {
                      columns: ':visible'
                    }
                  },
                  {
                    extend: 'excel',
                    exportOptions: {
                      columns: ':visible'
                    }
                  },
                  {
                    extend: 'print',
                    exportOptions: {
                      columns: ':visible'
                    }
                  }
                ],
                dom: 'Bflrtip',
                columnDefs:[
                  {
                    targets: [3,7,8,9,16,17,18,19],
                  },
              ],
              });

        tb.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
              that
                .search( this.value )
                .draw();
            }
          } );
        } );
      });

      $(function () {
        //Date picker
        $('#from_date').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          todayHighlight:'true'
        });
        $('#to_date').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd',
          defaultViewDate: 'today',
          endDate: '0d',
          todayHighlight:'true'
        });
      });

      $(document).ready(function(){
        $('#from_institute').val("<?php echo $from_institute ?>");
        $('#to_institute').val("<?php echo $to_institute ?>");
        $('#from_date').val("<?php echo $from_date ?>");
        $('#to_date').val("<?php echo $to_date ?>");
      });

      $(document).ready(function () {
        $('#l_rep').addClass('active');
        $('#l_rep2').addClass('active');
      });
    </script>

  </body>
</html>
