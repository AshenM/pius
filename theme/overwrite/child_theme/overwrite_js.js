// prevent form submit by keyboard enter

$('#search_form').on('keyup keypress', function(e) {
var keyCode = e.keyCode || e.which;
if (keyCode === 13) {
  e.preventDefault();
  return false;
}
});

//prevent form submit when intput field is empty


//ajax for data search
$(document).on('click', '#search-btn', function (e) {
  e.preventDefault();
  var view_key = $('#search_string').val();
  //removing whitespace from search_string
  var view_key= view_key.replace(/^\s+|\s+$/gm,'');

  var re = new RegExp("^[A-Z a-z]{1}[\/][0-9]{5}[\/][0-9]{4}$");
  if (re.test(view_key)) {
    $.ajax({
      url: $('#search_form').attr('action'),
      method: "POST",
      dataType: 'JSON',
      data: {view_key: view_key},
      success: function (data) {
        //console.log(data);
        $('#search_inmate_number').text(data.inmate_number);
        $('#search_inmate_name').text(data.inmate_name);
        $('#search_institute').text(data.institute);
        $('#search_prison_date').text(data.prison_date);
        if(data.active_state=='1'){
          $('#search_release').text('-');
        }else {
          $('#search_release').text(data.release_type+' ('+data.delete_date+')');
        }
      }
    });
    $('#searched_inmate').modal('toggle');
    $('#searched_inmate').modal('show');
  } else {
    $('#searched_inmate').modal('hide');
    swal.fire({
      type: 'error',
      title: 'සමාවන්න!',
      text: 'ඔබ ඇතුලත් කල සිර අංකය වැරදියි.'
    });
  }
  //console.log(view_key);

});
